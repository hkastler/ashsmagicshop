/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.mail;


import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.*;


/**
 *
 * @author henry.kastler
 */
@Stateless
public class EmailSessionBean {

    private String from = "henrykastler@gmail.com";
    private String to = "hkastler@gmail.com";
        
    @Inject
    Logger log;
    
    @Resource(mappedName="java:jboss/mail/Default")
    private Session mailSession;

    @Asynchronous
    public void sendEmail(String cc, String subject, String body) throws NoSuchProviderException, MessagingException{
        
        /*
        Properties props = new Properties();
        props.put("mail.smtps.auth", "true");
        props.put("mail.debug", "true");
        
        Session session = Session.getInstance(props);        
        */
        MimeMessage msg = new MimeMessage(mailSession);
        // set the message content here
        InternetAddress iAddrTo = new InternetAddress(to);
        InternetAddress iAddCc = new InternetAddress(cc);
        
        msg.setSentDate(new java.util.Date());
        msg.setFrom(from);
        msg.setRecipient(Message.RecipientType.TO, iAddrTo);
        msg.setRecipient(Message.RecipientType.CC, iAddCc);
        msg.setSubject(subject);
        msg.setText(body);
        
        Transport.send(msg);
        log.info("email sent");
        
        
    }
}
