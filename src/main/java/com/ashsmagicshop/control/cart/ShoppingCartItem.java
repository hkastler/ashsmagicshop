/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ashsmagicshop.control.cart;

import com.ashsmagicshop.entity.Product;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author henry.kastler
 */
@SessionScoped
public class ShoppingCartItem implements Serializable{

    Product product;
    int quantity;

    public ShoppingCartItem(Product product) {
        this.product = product;
        quantity = 1;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void incrementQuantity() {
        quantity++;
    }

    public void decrementQuantity() {
        quantity--;
    }

    public double getTotal() {
        double amount = 0;
        amount = (this.getQuantity() * product.getPrice().doubleValue());
        return amount;
    }

}