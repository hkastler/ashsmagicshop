/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.jsf.converter;

import com.ashsmagicshop.boundary.facade.ShippingServiceFacade;
import com.ashsmagicshop.boundary.cdi.CheckoutBean;
import com.ashsmagicshop.entity.ShippingService;
import java.math.BigDecimal;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@FacesConverter("com.ashsmagicshop.control.jsf.converter.ShippingServiceConverter")
public class ShippingServiceConverter implements Converter{

    @Inject
    ShippingServiceFacade ssf;
    
    @Inject
    private transient Logger log;
        
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
          int id = Integer.parseInt(value);
          log.info("id:"+ id);
          ShippingService ss = (ShippingService) ssf.find(id);//session.load(CatalogValue .class, id);
          log.info("ssId:" + ss.toString());
          return ss;
        } catch (Exception ex) {
            String message = ex.getMessage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message));
        } 

        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((ShippingService) o).getShippingServiceId().toString();
    }
    
    public ShippingServiceFacade getSsf() {
        return ssf;
    }

    public void setSsf(ShippingServiceFacade ssf) {
        this.ssf = ssf;
    }

    
   
    
}
