/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.jsf.converter;

import com.ashsmagicshop.boundary.facade.UserFacade;
import com.ashsmagicshop.entity.User;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;


@FacesConverter("ashsmagicshop.control.jsf.converter.UserConverter")
public class UserConverter implements Converter {
    
  @Inject
  UserFacade uf;

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
    
    try {
      int id = Integer.parseInt(value);
      User user = (User) uf.find(id);//session.load(CatalogValue .class, id);
      return user;
    } catch (Exception ex) {
     
     
      String message = ex.getMessage();
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message));
    } 

    return null;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    return ((User) value).getUserId() + "";
  }

}
