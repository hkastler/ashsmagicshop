/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.jsf.converter;


/**
 *
 * @author henry.kastler
 */

import com.ashsmagicshop.boundary.facade.JdbcrealmGroupFacade;
import com.ashsmagicshop.entity.JdbcrealmGroup;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;



@FacesConverter("ashsmagicshop.control.jsf.converter.JdbcrealmGroupConverter")
public class JdbcrealmGroupConverter implements Converter {
    
  @Inject
  JdbcrealmGroupFacade jgf;

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
    
    try {
      int id = Integer.parseInt(value);
      JdbcrealmGroup group = (JdbcrealmGroup) jgf.find(id);
      return group;
    } catch (Exception ex) {
     
     
      String message = ex.getMessage();
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message));
    } 

    return null;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    return ((JdbcrealmGroup) value).getJdbcrealmGroupId() + "";
  }

}

