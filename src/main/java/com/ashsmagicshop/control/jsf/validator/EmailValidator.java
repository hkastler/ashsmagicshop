/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.jsf.validator;

import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@FacesValidator("com.ashsmagicshop.control.jsf.validator.EmailValidator")
public class EmailValidator implements Validator{
    
    
    @Inject
    private transient Logger log;
    
    public EmailValidator(){
            
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        
        boolean isValidEmail = validateEmailAddress(value.toString());
        
        if(!isValidEmail){
            FacesMessage msg = 
                        new FacesMessage("Email not formatted correctly");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
        }

    }
    
    public boolean validateEmailAddress(String emailAddress){
        boolean isValidEmail = false;
        try{
            javax.mail.internet.InternetAddress m = new javax.mail.internet.InternetAddress(emailAddress);
            m.validate();
            isValidEmail = true;
        }catch(Exception e){
            log.fine("not valid email:" + emailAddress);
        }
        return isValidEmail;
    }
}
