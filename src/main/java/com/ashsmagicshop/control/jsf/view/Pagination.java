/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.jsf.view;

import javax.faces.model.DataModel;

/**
 *
 * @author henry.kastler
 */
public abstract class Pagination {
    
    private PaginationHelper pagination;
    private DataModel items = null;   
    private String view;
    private String outcome;
    private int page;
    private int pageSize;
    private int numberOfPages;

    public Pagination() {
       
    }
    
    public abstract PaginationHelper getPagination();
    
    public abstract void viewAction();
            

    public DataModel getItems() {
        return items;
    }

    public void setItems(DataModel items) {
        this.items = items;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
    
    public void setNumberOfPages(double itemCount, double pageSize) {
        double numP = Math.round( (itemCount / pageSize));
        this.numberOfPages = (int) numP;
    }

    public void setPagination(PaginationHelper pagination) {
        this.pagination = pagination;
    }
    
    
    
}
