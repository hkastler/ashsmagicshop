/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.util;

/**
 *
 * @author henry.kastler
 */
public enum CreditCard {
    AMERICAN_EXPRESS("American Express","AE"),
    DISCOVER("Discover","DV"),
    MASTERCARD("MasterCard","MC"), 
    VISA("Visa","VA");   
    
    String label;
    String code;
    
    CreditCard(String label, String code ){
        this.label = label;
        this.code = code;
    }
    
    public String getLabel(){
        return label;
    }
    
    public String getCode(){
        return code;
    }
}




