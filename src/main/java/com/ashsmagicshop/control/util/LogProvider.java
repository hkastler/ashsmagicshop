/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.control.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 *
 * @author henry.kastler
 */
public class LogProvider {
    @Produces
    public Logger createLogger(InjectionPoint ip){
        Logger logger = Logger.getLogger(ip.getMember().getDeclaringClass().getName());
        //logger.setLevel(Level.FINE);
        return logger;
        //private static final Logger log = Logger.getLogger(ip.getMember().getDeclaringClass().getName());
        
    }
}
