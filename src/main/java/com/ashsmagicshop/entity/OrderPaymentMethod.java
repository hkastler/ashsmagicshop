/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "orderpaymentmethod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderPaymentMethod.findAll", query = "SELECT o FROM OrderPaymentMethod o"),
    @NamedQuery(name = "OrderPaymentMethod.findByOrderPaymentMethodId", query = "SELECT o FROM OrderPaymentMethod o WHERE o.orderPaymentMethodId = :orderPaymentMethodId"),
    //@NamedQuery(name = "OrderPaymentMethod.findByOrderId", query = "SELECT o FROM OrderPaymentMethod o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "OrderPaymentMethod.findByPaymentmethodId", query = "SELECT o FROM OrderPaymentMethod o WHERE o.paymentmethodId = :paymentmethodId"),
    @NamedQuery(name = "OrderPaymentMethod.findByCcNumber", query = "SELECT o FROM OrderPaymentMethod o WHERE o.ccNumber = :ccNumber"),
    @NamedQuery(name = "OrderPaymentMethod.findByExpMonth", query = "SELECT o FROM OrderPaymentMethod o WHERE o.expMonth = :expMonth"),
    @NamedQuery(name = "OrderPaymentMethod.findByExpYear", query = "SELECT o FROM OrderPaymentMethod o WHERE o.expYear = :expYear"),
    @NamedQuery(name = "OrderPaymentMethod.findByVerificationCode", query = "SELECT o FROM OrderPaymentMethod o WHERE o.verificationCode = :verificationCode"),
    @NamedQuery(name = "OrderPaymentMethod.findByNameOnCard", query = "SELECT o FROM OrderPaymentMethod o WHERE o.nameOnCard = :nameOnCard"),
    @NamedQuery(name = "OrderPaymentMethod.findByOtherVerificationData", query = "SELECT o FROM OrderPaymentMethod o WHERE o.otherVerificationData = :otherVerificationData"),
    @NamedQuery(name = "OrderPaymentMethod.findByPaymentAmount", query = "SELECT o FROM OrderPaymentMethod o WHERE o.paymentAmount = :paymentAmount")})
public class OrderPaymentMethod implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orderpaymentmethodId")
    private Integer orderPaymentMethodId;
    
    @Column(name = "paymentmethodId")
    private Integer paymentmethodId;
    
    @Size(min=1,max = 16, message="{orderPaymentMethod.creditCardLength}")
    @Column(name = "cc_number")    
    private String ccNumber;
    
    
    @Min(1)
    @Max(12)
    @Column(name = "expMonth")
    private int expMonth;
    
    @Min(1)
    @Digits(integer=4,fraction=0)
    @Column(name = "expYear")
    private int expYear;
    
    @Size(max = 45)
    @Column(name = "verificationCode")
    private String verificationCode;
    
    @Size(min=1,max = 256, message="{orderPaymentMethod.nameOnCardSize}")
    @Column(name = "nameOnCard")
    private String nameOnCard;
    
    @Size(max = 45)
    @Column(name = "otherVerificationData")
    private String otherVerificationData;
    
    @Column(name = "paymentAmount")
    private BigDecimal paymentAmount;
    
    @ManyToOne
    @JoinColumn(name="customerorderId")
    private CustomerOrder customerOrder;

    public OrderPaymentMethod() {
    }

    public OrderPaymentMethod(Integer orderpaymentmethodId) {
        this.orderPaymentMethodId = orderpaymentmethodId;
    }
    
    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }
    
    public Integer getPaymentmethodId() {
        return paymentmethodId;
    }

    public void setPaymentmethodId(Integer paymentmethodId) {
        this.paymentmethodId = paymentmethodId;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getOtherVerificationData() {
        return otherVerificationData;
    }

    public void setOtherVerificationData(String otherVerificationData) {
        this.otherVerificationData = otherVerificationData;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderPaymentMethodId != null ? orderPaymentMethodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderPaymentMethod)) {
            return false;
        }
        OrderPaymentMethod other = (OrderPaymentMethod) object;
        if ((this.orderPaymentMethodId == null && other.orderPaymentMethodId != null) || (this.orderPaymentMethodId != null && !this.orderPaymentMethodId.equals(other.orderPaymentMethodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.OrderPaymentMethod[ orderpaymentmethodId=" + orderPaymentMethodId + " ]";
    }

    public Integer getOrderPaymentMethodId() {
        return orderPaymentMethodId;
    }

    public void setOrderPaymentMethodId(Integer orderPaymentMethodId) {
        this.orderPaymentMethodId = orderPaymentMethodId;
    }
    
}
