/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author henry.kastler
 */
@Embeddable
public class UserHasUserRolePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user__userId")
    private int useruserId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userrole__userroleId")
    private int userroleuserroleId;

    public UserHasUserRolePK() {
    }

    public UserHasUserRolePK(int useruserId, int userroleuserroleId) {
        this.useruserId = useruserId;
        this.userroleuserroleId = userroleuserroleId;
    }

    public int getUseruserId() {
        return useruserId;
    }

    public void setUseruserId(int useruserId) {
        this.useruserId = useruserId;
    }

    public int getUserroleuserroleId() {
        return userroleuserroleId;
    }

    public void setUserroleuserroleId(int userroleuserroleId) {
        this.userroleuserroleId = userroleuserroleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) useruserId;
        hash += (int) userroleuserroleId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasUserRolePK)) {
            return false;
        }
        UserHasUserRolePK other = (UserHasUserRolePK) object;
        if (this.useruserId != other.useruserId) {
            return false;
        }
        if (this.userroleuserroleId != other.userroleuserroleId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.UserHasUserRolePK[ useruserId=" + useruserId + ", userroleuserroleId=" + userroleuserroleId + " ]";
    }
    
}
