/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "shippingservice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ShippingService.findAll", query = "SELECT s FROM ShippingService s"),
    @NamedQuery(name = "ShippingService.findByShippingServiceId", query = "SELECT s FROM ShippingService s WHERE s.shippingServiceId = :shippingServiceId"),
    @NamedQuery(name = "ShippingService.findByName", query = "SELECT s FROM ShippingService s WHERE s.name = :name")})
public class ShippingService implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shippingserviceId")
    private Integer shippingServiceId;

    
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    
    @Column(name = "cost")
    private BigDecimal cost;
    
    @Transient
    private String label;

   

    public ShippingService() {
    }

    public ShippingService(Integer shippingServiceId) {
        this.shippingServiceId = shippingServiceId;
    }
    
    public ShippingService (String name, BigDecimal cost){
        this.name = name;
        this.cost = cost;
    }
    
    public ShippingService (int id, String name, BigDecimal cost){
        this.shippingServiceId = id;
        this.name = name;
        this.cost = cost;
    }

    public Integer getShippingServiceId() {
        return shippingServiceId;
    }

    public void setShippingServiceId(Integer shippingServiceId) {
        this.shippingServiceId = shippingServiceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
    
     public String getLabel() {
        return label;
    }

    public void setLabel() {
        this.label = getName().concat("-").concat(getCost().toString());
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shippingServiceId != null ? shippingServiceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShippingService)) {
            return false;
        }
        ShippingService other = (ShippingService) object;
        if ((this.shippingServiceId == null && other.shippingServiceId != null) || (this.shippingServiceId != null && !this.shippingServiceId.equals(other.shippingServiceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.ShippingService[ shippingserviceId=" + shippingServiceId + " ]";
    }
    
}
