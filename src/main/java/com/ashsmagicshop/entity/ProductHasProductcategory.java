/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "product_has_productcategory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductHasProductcategory.findAll", query = "SELECT p FROM ProductHasProductcategory p"),
    @NamedQuery(name = "ProductHasProductcategory.findByProductproductId", query = "SELECT p FROM ProductHasProductcategory p WHERE p.productHasProductcategoryPK.productproductId = :productproductId"),
    @NamedQuery(name = "ProductHasProductcategory.findByProductcategoryproductCategoryId", query = "SELECT p FROM ProductHasProductcategory p WHERE p.productHasProductcategoryPK.productcategoryproductCategoryId = :productcategoryproductCategoryId")})
public class ProductHasProductcategory implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductHasProductcategoryPK productHasProductcategoryPK;

    public ProductHasProductcategory() {
    }

    public ProductHasProductcategory(ProductHasProductcategoryPK productHasProductcategoryPK) {
        this.productHasProductcategoryPK = productHasProductcategoryPK;
    }

    public ProductHasProductcategory(int productproductId, int productcategoryproductCategoryId) {
        this.productHasProductcategoryPK = new ProductHasProductcategoryPK(productproductId, productcategoryproductCategoryId);
    }

    public ProductHasProductcategoryPK getProductHasProductcategoryPK() {
        return productHasProductcategoryPK;
    }

    public void setProductHasProductcategoryPK(ProductHasProductcategoryPK productHasProductcategoryPK) {
        this.productHasProductcategoryPK = productHasProductcategoryPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productHasProductcategoryPK != null ? productHasProductcategoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductHasProductcategory)) {
            return false;
        }
        ProductHasProductcategory other = (ProductHasProductcategory) object;
        if ((this.productHasProductcategoryPK == null && other.productHasProductcategoryPK != null) || (this.productHasProductcategoryPK != null && !this.productHasProductcategoryPK.equals(other.productHasProductcategoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.entity.ProductHasProductcategory[ productHasProductcategoryPK=" + productHasProductcategoryPK + " ]";
    }
    
}
