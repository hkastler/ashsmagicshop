/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "user__has_userrole_")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserHasUserRole.findAll", query = "SELECT u FROM UserHasUserRole u"),
    @NamedQuery(name = "UserHasUserRole.findByUseruserId", query = "SELECT u FROM UserHasUserRole u WHERE u.userHasUserRolePK.useruserId = :useruserId"),
    @NamedQuery(name = "UserHasUserRole.findByUserroleuserroleId", query = "SELECT u FROM UserHasUserRole u WHERE u.userHasUserRolePK.userroleuserroleId = :userroleuserroleId")})
public class UserHasUserRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserHasUserRolePK userHasUserRolePK;

    public UserHasUserRole() {
    }

    public UserHasUserRole(UserHasUserRolePK userHasUserRolePK) {
        this.userHasUserRolePK = userHasUserRolePK;
    }

    public UserHasUserRole(int useruserId, int userroleuserroleId) {
        this.userHasUserRolePK = new UserHasUserRolePK(useruserId, userroleuserroleId);
    }

    public UserHasUserRolePK getUserHasUserRolePK() {
        return userHasUserRolePK;
    }

    public void setUserHasUserRolePK(UserHasUserRolePK userHasUserRolePK) {
        this.userHasUserRolePK = userHasUserRolePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHasUserRolePK != null ? userHasUserRolePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasUserRole)) {
            return false;
        }
        UserHasUserRole other = (UserHasUserRole) object;
        if ((this.userHasUserRolePK == null && other.userHasUserRolePK != null) || (this.userHasUserRolePK != null && !this.userHasUserRolePK.equals(other.userHasUserRolePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.UserHasUserRole[ userHasUserRolePK=" + userHasUserRolePK + " ]";
    }
    
}
