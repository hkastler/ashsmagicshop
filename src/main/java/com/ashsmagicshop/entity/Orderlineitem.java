/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "orderlineitem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orderlineitem.findAll", query = "SELECT o FROM Orderlineitem o"),
    @NamedQuery(name = "Orderlineitem.findByOrderlineitemid", query = "SELECT o FROM Orderlineitem o WHERE o.orderlineitemid = :orderlineitemid"),
    @NamedQuery(name = "Orderlineitem.findByCustomerorderId", query = "SELECT o FROM Orderlineitem o WHERE o.customerorderId = :customerorderId"),
    @NamedQuery(name = "Orderlineitem.findByProductid", query = "SELECT o FROM Orderlineitem o WHERE o.productid = :productid"),
    @NamedQuery(name = "Orderlineitem.findByQuantity", query = "SELECT o FROM Orderlineitem o WHERE o.quantity = :quantity"),
    @NamedQuery(name = "Orderlineitem.findByItemPrice", query = "SELECT o FROM Orderlineitem o WHERE o.itemPrice = :itemPrice"),
    @NamedQuery(name = "Orderlineitem.findByTotalPrice", query = "SELECT o FROM Orderlineitem o WHERE o.totalPrice = :totalPrice"),
    @NamedQuery(name = "Orderlineitem.findByItemStatusCode", query = "SELECT o FROM Orderlineitem o WHERE o.itemStatusCode = :itemStatusCode")})
public class Orderlineitem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orderlineitemid")
    private Integer orderlineitemid;
    @Column(name = "customerorderId")
    private Integer customerorderId;
    @Column(name = "productid")
    private Integer productid;
    @Column(name = "quantity")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "itemPrice")
    private BigDecimal itemPrice;
    @Column(name = "totalPrice")
    private BigDecimal totalPrice;
    @Size(max = 45)
    @Column(name = "itemStatusCode")
    private String itemStatusCode;

    public Orderlineitem() {
    }

    public Orderlineitem(Integer orderlineitemid) {
        this.orderlineitemid = orderlineitemid;
    }

    public Integer getOrderlineitemid() {
        return orderlineitemid;
    }

    public void setOrderlineitemid(Integer orderlineitemid) {
        this.orderlineitemid = orderlineitemid;
    }

    public Integer getCustomerorderId() {
        return customerorderId;
    }

    public void setCustomerorderId(Integer customerorderId) {
        this.customerorderId = customerorderId;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getItemStatusCode() {
        return itemStatusCode;
    }

    public void setItemStatusCode(String itemStatusCode) {
        this.itemStatusCode = itemStatusCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderlineitemid != null ? orderlineitemid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orderlineitem)) {
            return false;
        }
        Orderlineitem other = (Orderlineitem) object;
        if ((this.orderlineitemid == null && other.orderlineitemid != null) || (this.orderlineitemid != null && !this.orderlineitemid.equals(other.orderlineitemid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.Orderlineitem[ orderlineitemid=" + orderlineitemid + " ]";
    }
    
}
