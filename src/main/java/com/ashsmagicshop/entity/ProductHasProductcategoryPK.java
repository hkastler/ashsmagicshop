/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author henry.kastler
 */
@Embeddable
public class ProductHasProductcategoryPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "product_productId")
    private int productproductId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "productcategory_productCategoryId")
    private int productcategoryproductCategoryId;

    public ProductHasProductcategoryPK() {
    }

    public ProductHasProductcategoryPK(int productproductId, int productcategoryproductCategoryId) {
        this.productproductId = productproductId;
        this.productcategoryproductCategoryId = productcategoryproductCategoryId;
    }

    public int getProductproductId() {
        return productproductId;
    }

    public void setProductproductId(int productproductId) {
        this.productproductId = productproductId;
    }

    public int getProductcategoryproductCategoryId() {
        return productcategoryproductCategoryId;
    }

    public void setProductcategoryproductCategoryId(int productcategoryproductCategoryId) {
        this.productcategoryproductCategoryId = productcategoryproductCategoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productproductId;
        hash += (int) productcategoryproductCategoryId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductHasProductcategoryPK)) {
            return false;
        }
        ProductHasProductcategoryPK other = (ProductHasProductcategoryPK) object;
        if (this.productproductId != other.productproductId) {
            return false;
        }
        if (this.productcategoryproductCategoryId != other.productcategoryproductCategoryId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.entity.ProductHasProductcategoryPK[ productproductId=" + productproductId + ", productcategoryproductCategoryId=" + productcategoryproductCategoryId + " ]";
    }
    
}
