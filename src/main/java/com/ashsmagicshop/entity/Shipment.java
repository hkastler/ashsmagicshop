/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "shipment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shipment.findAll", query = "SELECT s FROM Shipment s"),
    @NamedQuery(name = "Shipment.findByShipmentId", query = "SELECT s FROM Shipment s WHERE s.shipmentId = :shipmentId"),
    @NamedQuery(name = "Shipment.findByCustomerorderId", query = "SELECT s FROM Shipment s WHERE s.customerorderId = :customerorderId"),
    @NamedQuery(name = "Shipment.findByShippingserviceId", query = "SELECT s FROM Shipment s WHERE s.shippingserviceId = :shippingserviceId")})
public class Shipment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shipmentId")
    private Integer shipmentId;
    @Column(name = "customerorderId")
    private Integer customerorderId;
    @Column(name = "shippingserviceId")
    private Integer shippingserviceId;

    public Shipment() {
    }

    public Shipment(Integer shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Integer getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Integer shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Integer getCustomerorderId() {
        return customerorderId;
    }

    public void setCustomerorderId(Integer customerorderId) {
        this.customerorderId = customerorderId;
    }

    public Integer getShippingserviceId() {
        return shippingserviceId;
    }

    public void setShippingserviceId(Integer shippingserviceId) {
        this.shippingserviceId = shippingserviceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shipmentId != null ? shipmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shipment)) {
            return false;
        }
        Shipment other = (Shipment) object;
        if ((this.shipmentId == null && other.shipmentId != null) || (this.shipmentId != null && !this.shipmentId.equals(other.shipmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.Shipment[ shipmentId=" + shipmentId + " ]";
    }
    
}
