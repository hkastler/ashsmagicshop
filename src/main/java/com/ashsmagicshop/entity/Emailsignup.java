/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "emailsignup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Emailsignup.findAll", query = "SELECT e FROM Emailsignup e"),
    @NamedQuery(name = "Emailsignup.findByEmailSignUpId", query = "SELECT e FROM Emailsignup e WHERE e.emailSignUpId = :emailSignUpId"),
    @NamedQuery(name = "Emailsignup.findByEmailAddress", query = "SELECT e FROM Emailsignup e WHERE e.emailAddress = :emailAddress"),
    @NamedQuery(name = "Emailsignup.findByFirstName", query = "SELECT e FROM Emailsignup e WHERE e.firstName = :firstName"),
    @NamedQuery(name = "Emailsignup.findByLastName", query = "SELECT e FROM Emailsignup e WHERE e.lastName = :lastName")})
public class Emailsignup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer emailSignUpId;
    @Size(max = 255)
    @Column(length = 255)
    private String emailAddress;
    @Size(max = 45)
    @Column(length = 45)
    private String firstName;
    @Size(max = 45)
    @Column(length = 45)
    private String lastName;

    public Emailsignup() {
    }

    public Emailsignup(Integer emailSignUpId) {
        this.emailSignUpId = emailSignUpId;
    }

    public Integer getEmailSignUpId() {
        return emailSignUpId;
    }

    public void setEmailSignUpId(Integer emailSignUpId) {
        this.emailSignUpId = emailSignUpId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emailSignUpId != null ? emailSignUpId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Emailsignup)) {
            return false;
        }
        Emailsignup other = (Emailsignup) object;
        if ((this.emailSignUpId == null && other.emailSignUpId != null) || (this.emailSignUpId != null && !this.emailSignUpId.equals(other.emailSignUpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.entity.Emailsignup[ emailSignUpId=" + emailSignUpId + " ]";
    }
    
}
