/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author henry.kastler
 */
@Entity
@Table(name = "customerorder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomerOrder.findAll", query = "SELECT c FROM CustomerOrder c"),
    @NamedQuery(name = "CustomerOrder.findByCustomerorderId", query = "SELECT c FROM CustomerOrder c WHERE c.customerorderId = :customerorderId"),
    @NamedQuery(name = "CustomerOrder.findByCustomerId", query = "SELECT c FROM CustomerOrder c WHERE c.customerId = :customerId"),
    @NamedQuery(name = "CustomerOrder.findByOrderDate", query = "SELECT c FROM CustomerOrder c WHERE c.orderDate = :orderDate"),
    @NamedQuery(name = "CustomerOrder.findByBillTofirstName", query = "SELECT c FROM CustomerOrder c WHERE c.billTofirstName = :billTofirstName"),
    @NamedQuery(name = "CustomerOrder.findByBillTolastName", query = "SELECT c FROM CustomerOrder c WHERE c.billTolastName = :billTolastName"),
    @NamedQuery(name = "CustomerOrder.findByBillToemailAddress", query = "SELECT c FROM CustomerOrder c WHERE c.billToemailAddress = :billToemailAddress"),
    @NamedQuery(name = "CustomerOrder.findByShipTofirstName", query = "SELECT c FROM CustomerOrder c WHERE c.shipTofirstName = :shipTofirstName"),
    @NamedQuery(name = "CustomerOrder.findByShipTolastName", query = "SELECT c FROM CustomerOrder c WHERE c.shipTolastName = :shipTolastName"),
    @NamedQuery(name = "CustomerOrder.findBySubTotal", query = "SELECT c FROM CustomerOrder c WHERE c.subTotal = :subTotal"),
    @NamedQuery(name = "CustomerOrder.findByTaxAmount", query = "SELECT c FROM CustomerOrder c WHERE c.taxAmount = :taxAmount"),
    @NamedQuery(name = "CustomerOrder.findByShippingCharge", query = "SELECT c FROM CustomerOrder c WHERE c.shippingCharge = :shippingCharge"),
    @NamedQuery(name = "CustomerOrder.findByGrandTotal", query = "SELECT c FROM CustomerOrder c WHERE c.grandTotal = :grandTotal"),
    @NamedQuery(name = "CustomerOrder.findByPromotionCode", query = "SELECT c FROM CustomerOrder c WHERE c.promotionCode = :promotionCode"),
    
    @NamedQuery(name = "CustomerOrder.findByOrderStatusId", query = "SELECT c FROM CustomerOrder c WHERE c.orderStatusId = :orderStatusId")})
public class CustomerOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "customerorderId")
    private Integer customerorderId;

    @Column(name = "customerId")
    private Integer customerId;

    @Basic(optional = false)
    //@NotNull(message = "customerOrder.orderDateNull")
    @Column(name = "orderDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;

    @Size(min=1,max=128,message="{customerOrder.billingFirstNameError}")
    @Column(name = "billTo_firstName")
    private String billTofirstName;

    @Size(min=1,max=128,message="{customerOrder.billingLastNameError}")
    @Column(name = "billTo_lastName")
    private String billTolastName;

    @Size(min=1,max = 45,message="{customerOrder.billingEmailAddressError}")
    @Column(name = "billTo_emailAddress")
    private String billToemailAddress;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "billTo_addressId")
    private Address billingAddress = new Address();

    @Size(min=1,max = 45, message="{customerOrder.shipTofirstNameError}")
    @Column(name = "shipTo_firstName")
    private String shipTofirstName;

    @Size(min=1,max = 45, message="{customerOrder.shipTolastNameError}")
    @Column(name = "shipTo_lastName")
    private String shipTolastName;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "customerorder_shipping_addresses",
            joinColumns = @JoinColumn(name = "customerOrderId",referencedColumnName = "customerorderid"), 
            inverseJoinColumns = @JoinColumn(name = "addressId",referencedColumnName = "addressid"))
    private List<Address> shippingAddresses;
    
    @OneToOne
    @JoinColumn(name = "shippingServiceId")
    private ShippingService shippingService = new ShippingService();
    
    @OneToMany(cascade=CascadeType.ALL,mappedBy="customerOrder")
    private List<OrderPaymentMethod> orderPaymentMethods;
    
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "subTotal")
    private BigDecimal subTotal = new BigDecimal(0);

    @Column(name = "taxAmount")
    private BigDecimal taxAmount = new BigDecimal(0);

    @Column(name = "shippingCharge")
    private BigDecimal shippingCharge = new BigDecimal(0);

    @Column(name = "grandTotal")
    private BigDecimal grandTotal = new BigDecimal(0);

    @Size(max = 45)
    @Column(name = "promotionCode")
    private String promotionCode;
    
    @Column(name="comments")
    private String comments;

    @Column(name = "orderStatusId")
    private Integer orderStatusId;
    
    private static final Logger log = Logger.getLogger(CustomerOrder.class.getName());

    public CustomerOrder() {
    }

    public CustomerOrder(Integer customerorderId) {
        this.customerorderId = customerorderId;
    }

    public CustomerOrder(Integer customerorderId, Date orderDate) {
        this.customerorderId = customerorderId;
        this.orderDate = orderDate;
    }

    public Integer getCustomerorderId() {
        return customerorderId;
    }

    public void setCustomerorderId(Integer customerorderId) {
        this.customerorderId = customerorderId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getBillTofirstName() {
        return billTofirstName;
    }

    public void setBillTofirstName(String billTofirstName) {
        this.billTofirstName = billTofirstName;
    }

    public String getBillTolastName() {
        return billTolastName;
    }

    public void setBillTolastName(String billTolastName) {
        this.billTolastName = billTolastName;
    }

    public String getBillToemailAddress() {
        return billToemailAddress;
    }

    public void setBillToemailAddress(String billToemailAddress) {
        this.billToemailAddress = billToemailAddress;
    }

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }
//
    public List<Address> getShippingAddresses() {
       if(shippingAddresses == null){
           shippingAddresses = new ArrayList<Address>();
       }
       return shippingAddresses;
    }
///
    public void setShippingAddresses(List<Address> shippingAddresses) {
       this.shippingAddresses = shippingAddresses;
    }

    public String getShipTofirstName() {
        return shipTofirstName;
    }

    public void setShipTofirstName(String shipTofirstName) {
        this.shipTofirstName = shipTofirstName;
    }

    public String getShipTolastName() {
        return shipTolastName;
    }

    public void setShipTolastName(String shipTolastName) {
        this.shipTolastName = shipTolastName;
    }

    public BigDecimal getSubTotal() {
        log.info("getting subTotal");
        if(subTotal==null){
            log.info("subTotal is null");
            subTotal = new BigDecimal(0);
        }else{
            log.log(Level.INFO, "subTotal is:{0}", subTotal.toString());
        }
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getTaxAmount() {
        if(taxAmount == null){taxAmount = new BigDecimal(0);}
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getShippingCharge() {
        if(shippingCharge == null){ shippingCharge = new BigDecimal(0);}
        return shippingCharge;
    }

    public void setShippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }
    
    public BigDecimal calcGrandTotal(BigDecimal subTotal, BigDecimal shippingCharge,
            BigDecimal taxAmount){
        BigDecimal newGrandTotal = new BigDecimal(0);
        //log.log(Level.INFO, "{0} {1} {2}", new Object[]{subTotal, shippingCharge, taxAmount});
        newGrandTotal = newGrandTotal.add(subTotal).add(shippingCharge).add(taxAmount);
        log.log(Level.INFO, "newGrandTotal:'{'0'}'{0}", newGrandTotal.toString());
        return newGrandTotal;
    }
    
    public void recalcGrandTotal(){
        log.info("WTF");
        BigDecimal newGrandTotal = new BigDecimal(0);
        log.log(Level.INFO, "{0} {1} {2}", new Object[]{subTotal, shippingCharge, taxAmount});
        newGrandTotal = newGrandTotal.add(subTotal).add(shippingCharge).add(taxAmount);
        log.log(Level.INFO, "newGrandTotal:{0}", newGrandTotal);
        setGrandTotal(newGrandTotal);
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }
    
    public List<OrderPaymentMethod> getOrderPaymentMethods() {
        if(orderPaymentMethods == null){
            orderPaymentMethods = new ArrayList<OrderPaymentMethod>();
        }
        return orderPaymentMethods;
    }

    public void setOrderPaymentMethods(List<OrderPaymentMethod> orderPaymentMethods) {
        this.orderPaymentMethods = orderPaymentMethods;
    }

    public Integer getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Integer orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerorderId != null ? customerorderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerOrder)) {
            return false;
        }
        CustomerOrder other = (CustomerOrder) object;
        if ((this.customerorderId == null && other.customerorderId != null) || (this.customerorderId != null && !this.customerorderId.equals(other.customerorderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ashsmagicshop.CustomerOrder[ customerorderId=" + customerorderId + " ]";
    }

    public ShippingService getShippingService() {
        return shippingService;
    }

    public void setShippingService(ShippingService shippingService) {
        this.shippingService = shippingService;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
