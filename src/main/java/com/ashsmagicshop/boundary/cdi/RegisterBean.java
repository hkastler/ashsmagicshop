/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.EmailsignupFacade;
import com.ashsmagicshop.boundary.facade.JdbcrealmGroupFacade;
import com.ashsmagicshop.boundary.facade.UserFacade;
import com.ashsmagicshop.control.util.PasswordUtil;
import com.ashsmagicshop.entity.JdbcrealmGroup;
import com.ashsmagicshop.entity.User;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@ManagedBean
@RequestScoped
public class RegisterBean {
    
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String confirmPassword;
    private String emailAddress;
    private boolean emailOptIn;
    private boolean isRegistered;
    
    @Inject
    UserFacade uf;
    
    @Inject
    JdbcrealmGroupFacade jgf;
    
    @Inject
    EmailsignupFacade esf;
    
    /**
     * Creates a new instance of RegisterBean
     */
    public RegisterBean() {
    }
    
    @PostConstruct
    public void init(){
        setIsRegistered(false);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public boolean isIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }
    
    /**
     * <p>Creates a new <code>user</code>.  If the specified user name exists
     * or an error occurs when persisting the user instance, enqueue a message
     * detailing the problem to the <code>FacesContext</code>.  If the 
     * user is created, move the user back to the login view.</p>
     *
     * @return <code>login</code> if the user is created, otherwise
     *  returns <code>null</code>
     */
    public String registerUser() {
        
        FacesContext context = FacesContext.getCurrentInstance();
        User wuser = uf.findByUsername(username);
        if (wuser == null) {
            if (!password.equals(confirmPassword)) {
                FacesMessage message = new FacesMessage("The specified passwords do not match.  Please try again");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage(null, message);
                return null;
            }
            wuser = new User();
            wuser.setFirstName(firstName);
            wuser.setLastName(lastName);
            try{
                String digestedPassword = PasswordUtil.getDigestedPassword(password);
                wuser.setPassword(digestedPassword);
            }catch(Exception e){
                wuser.setPassword(password);
            }
           
            wuser.setUsername(username);
            //wuser.setSince(new Date());
            wuser.setJdbcrealmGroups(getJdbcrealmGroupForUsers());
            
            try {
                uf.create(wuser);
                
                if(emailOptIn){
                    esf.create(firstName,lastName,emailAddress);
                }
                FacesMessage message = new FacesMessage("Login now.");
                context.addMessage(null, message);
                setIsRegistered(true);
                return "post-create-login";
            } catch (Exception e) {               
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                        "Error creating user!",
                                                        "Unexpected error when creating your account.  Please contact the system Administrator");
                context.addMessage(null, message);
                Logger.getAnonymousLogger().log(Level.SEVERE,
                                                "Unable to create new user",
                                                e);
                return null;
            }
        } else {           
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                    "Username '"
                                                      + username 
                                                      + "' already exists!  ",
                                                    "Please choose a different username.");
            context.addMessage("username",message);
            return null;
        }        
    }
    
    private List<JdbcrealmGroup> getJdbcrealmGroupForUsers(){
        List<JdbcrealmGroup> jg = jgf.findByName("user");
        return jg;
    }
}
