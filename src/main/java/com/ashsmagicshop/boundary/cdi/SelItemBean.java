/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.control.util.CreditCard;
import com.ashsmagicshop.control.util.US;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;


/**
 *
 * @author henry.kastler
 */
@Named(value = "selItBean")
@ApplicationScoped
public class SelItemBean {

    private SelectItem[] usItems;
    private SelectItem[] countryItems;
    private SelectItem[] creditCardItems;
    private SelectItem[] monthItems;    
    private SelectItem[] yearItems;

    @Inject
    private transient Logger log;
  
    /**
     * Creates a new instance of USBean
     */
    public SelItemBean() {
    }
    
    @PostConstruct
    private void init(){
        log.fine("SelItemBean init");
        //set this var just once for the application
        usItems = getUSSelectItems();
        countryItems = getCountrySelectItems();
        creditCardItems = getCreditCardSelectItems();
        monthItems = getMonthSelectItems();
        yearItems = getYearSelectItems();
    }
    
    private SelectItem[] getUSSelectItems(){
        log.log(Level.FINE, "getUSSelectItems:{0}", US.values().length);
        SelectItem[] items = new SelectItem[US.values().length];
        int i = 0;
        for (US us: US.values()){
            log.log(Level.FINE,us.getANSIabbreviation());
            log.log(Level.FINE, "here?{0}", us.getUnabbreviated());
            
            items[i++] = new SelectItem(us.getANSIabbreviation(),us.getUnabbreviated());
        }
        return items;
    }
    private SelectItem[] getCountrySelectItems() {
       
       String[] locales = Locale.getISOCountries(); 
       
       List<SelectItem> ftLocale = new ArrayList<SelectItem>();
        
       for (String countryCode : locales) { 
		Locale obj = new Locale("", countryCode); 
                ftLocale.add(new SelectItem(obj.getCountry(),obj.getDisplayCountry()) );               
        }
              
        log.log(Level.INFO, "ftLocale size:{0}", ftLocale.size());
        
       
        Comparator<SelectItem> localeNameSort = new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem si1, SelectItem si2) {
                        return  si1.getLabel().substring(0,1).compareTo(si2.getLabel().substring(0, 1));
                }
        };
                
        Collections.sort(ftLocale,localeNameSort);
       
        SelectItem[] cItems = ftLocale.toArray(new SelectItem[ftLocale.size()]);
        return cItems;
    }

    public SelectItem[] getUsItems() {
        return usItems;
    }

    public void setUsItems(SelectItem[] usItems) {
        this.usItems = usItems;
    }
    
     public SelectItem[] getCountryItems() {
        return countryItems;
    }

    public void setCountryItems(SelectItem[] countryItems) {
        this.countryItems = countryItems;
    }

     public SelectItem[] getCreditCardItems() {
        return creditCardItems;
    }

    public void setCreditCardItems(SelectItem[] creditCardItems) {
        this.creditCardItems = creditCardItems;
    }
    
    private SelectItem[] getCreditCardSelectItems() {
        SelectItem[] items = new SelectItem[CreditCard.values().length];
        int i = 0;
        for (CreditCard cc: CreditCard.values()){
            log.finest(cc.getCode());
            log.log(Level.FINEST, "here?{0}", cc.getLabel());
            items[i++] = new SelectItem(cc.getCode(),cc.getLabel());
        }
        return items;
    }
    
    public SelectItem[] getMonthItems() {
        return monthItems;
    }

    public void setMonthItems(SelectItem[] monthItems) {
        this.monthItems = monthItems;
    }

    public SelectItem[] getYearItems() {
        return yearItems;
    }

    public void setYearItems(SelectItem[] yearItems) {
        this.yearItems = yearItems;
    }

    private SelectItem[] getMonthSelectItems() {
        SelectItem[] items = new SelectItem[12];
        
        for(int i=0; i<=11; i++){
            String month = getMonth(i+1);
            String strMonth = Integer.toString(i+1);
            if(strMonth.length()==1){
                strMonth= "0"+strMonth;
            }
            items[i] = new SelectItem(i+1,strMonth);
        }
        
        return items;
    }
    
    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
    
    private SelectItem[] getYearSelectItems() {
        int currentYear;
        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        log.log(Level.INFO, "Year:{0}", currentYear);
        int yearsToAppear = 10;
        SelectItem[] items = new SelectItem[yearsToAppear];
        int counter = 0;
        for(int i=currentYear; i<currentYear+yearsToAppear; i++){
            items[counter] = new SelectItem(i,Integer.toString(i));
            counter++;
        }
        
        return items;
    }
    
    
}
