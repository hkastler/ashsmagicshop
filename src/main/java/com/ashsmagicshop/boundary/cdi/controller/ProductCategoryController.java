package com.ashsmagicshop.boundary.cdi.controller;

import com.ashsmagicshop.entity.ProductCategory;
import com.ashsmagicshop.boundary.cdi.controller.util.JsfUtil;
import com.ashsmagicshop.control.jsf.view.PaginationHelper;
import com.ashsmagicshop.boundary.facade.ProductCategoryFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;


@ManagedBean(name="productCategoryController")
@SessionScoped
public class ProductCategoryController extends AbstractController implements Serializable {


    private ProductCategory current;
    private DataModel items = null;
    @Inject private com.ashsmagicshop.boundary.facade.ProductCategoryFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<ProductCategory> productCategories = new ArrayList<ProductCategory>();
    
    @Inject
    private transient Logger log;

    public ProductCategoryController() {
    }
    
    @PostConstruct
    private void init(){
        setProductCategories();
        setView("List.xhtml");
    }

    public ProductCategory getSelected() {
        if (current == null) {
            current = new ProductCategory();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    public List<ProductCategory> getProductCategories(){
        return productCategories;
    }
    
   public void setProductCategories(){
       this.productCategories = ejbFacade.findAll();
   }

    private ProductCategoryFacade getFacade() {
        return ejbFacade;
    }
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem()+getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public void prepareList() {
        recreateModel();
        log.info("prepareList here");
        setView("List.xhtml");
        //return "List";
    }

    public void prepareView() {
        current = (ProductCategory)getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        setView("View.xhtml");
       
    }

    public void prepareCreate() {
        current = new ProductCategory();
        selectedItemIndex = -1;
        setView("Create.xhtml");
        
    }

    public void create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/messages").getString("ProductCategoryCreated"));
            setView("Create.xhtml");
            //return "Create";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/messages").getString("PersistenceErrorOccured"));
            setView("Create.xhtml");
            //return null;
        }
    }

    public void prepareEdit() {
        current = (ProductCategory)getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        setView("Edit.xhtml");
        //return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/messages").getString("ProductCategoryUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (ProductCategory)getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/messages").getString("ProductCategoryDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/messages").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count-1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex+1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @Override
    public void viewAction() {
        setView(getView());
    }


    @FacesConverter(forClass=ProductCategory.class)
    public static class ProductCategoryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProductCategoryController controller = (ProductCategoryController)facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "productCategoryController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ProductCategory) {
                ProductCategory o = (ProductCategory) object;
                return getStringKey(o.getProductCategoryId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: "+ProductCategory.class.getName());
            }
        }

    }

}
