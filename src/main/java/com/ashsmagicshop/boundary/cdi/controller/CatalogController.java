package com.ashsmagicshop.boundary.cdi.controller;

import com.ashsmagicshop.entity.Product;
import com.ashsmagicshop.boundary.cdi.controller.util.JsfUtil;
import com.ashsmagicshop.control.jsf.view.PaginationHelper;
import com.ashsmagicshop.boundary.facade.ProductFacade;
import com.ashsmagicshop.control.jsf.view.Pagination;
import com.ashsmagicshop.entity.ProductCategory;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import com.ashsmagicshop.boundary.facade.ProductCategoryFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;


@RequestScoped
@Named( value = "catalogController")
public class CatalogController extends Pagination implements Serializable {

    private Product current;
    private DataModel items = null;
    @Inject
    private com.ashsmagicshop.boundary.facade.ProductFacade productFacade;
    
    @Inject
    private ProductCategoryFacade pCatFacade;
    
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private int page;    
    private int pageSize;    
    private int numberOfPages;    
    private String orderBy;        
    private String orderByField;    
    private String shopSort;    
    private String view;
    
    @Inject
    private transient Logger log;

    public CatalogController() {
    }
    
    @PostConstruct
    public void init(){
        log.info("init called");
        page=1;
        pageSize=10;
        orderBy="asc";
        orderByField="productId";
        shopSort="name_asc";
        view="List.xhtml";
    }

    private ProductFacade getFacade() {
        return productFacade;
    }

    @Override
    public PaginationHelper getPagination() {
        
        if (pagination == null) {
            log.fine("paginationNull");
            pagination = new PaginationHelper(page-1,pageSize) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    
                    int lastRangeItem = getPageFirstItem() + getPageSize()-1;
                    if(getPageFirstItem() == 0){
                        lastRangeItem = pageSize-1;
                    }
                    log.log(Level.FINE, "firstRangeItem:{0}", getPageFirstItem());
                    log.log(Level.FINE, "lastRangeItem:{0}", lastRangeItem);
                    log.log(Level.FINE, "orderBy:{0}", orderBy);
                    log.log(Level.FINE, "orderByField:{0}", orderByField);
                    log.log(Level.FINE, "page:{0}", getPage());
                    log.log(Level.FINE, "pageSize:{0}", pageSize);
                    log.log(Level.FINE, "itemsCount:{0}", getItemsCount());
                    return new ListDataModel(
                            getFacade().findRange(
                                    new int[]{getPageFirstItem(), lastRangeItem},
                                    orderBy,
                                    orderByField));
                }
                
            };
        }
        //casting the ints to doubles is important!
        //.5s weren't rounding up without it
        double numberOfPages = Math.round( ((double)pagination.getItemsCount()/ (double)pageSize));
        log.log(Level.FINE, "numberOfPages:{0}", numberOfPages);
        log.log(Level.FINE, "itemsCount:{0}", pagination.getItemsCount());
        log.log(Level.FINE, "pageSize:{0}", pageSize);
        setNumberOfPages((int) numberOfPages);
        return pagination;
    }
   
    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "shop";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "shop";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(productFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(productFacade.findAll(), true);
    }

    @Override
    public void viewAction() {
        log.log(Level.FINE, "content:{0}", this.view);
        log.log(Level.FINE, "page:{0}", this.page);
        log.log(Level.FINE, "getPagination.getPage:{0}", getPagination().getPage());
        items = getPagination().createPageDataModel();
    }

    @FacesConverter(forClass = Product.class)
    public static class CatalogControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CatalogController controller = (CatalogController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "catalogController");
            return controller.productFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Product) {
                Product o = (Product) object;
                return getStringKey(o.getProductId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Product.class.getName());
            }
        }

    }
    
    public List<Product> findAll(){
        log.info("finding all products");
        List<Product> products = productFacade.findAll();
        return products;
    }
    
    public int getNumberOfProducts(){
        int num = productFacade.count();
        return num;
    }

    public int getPageSize() {
        return pageSize;
    }

   
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    
    

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }

    public String getShopSort() {
        return shopSort;
    }

    public void setShopSort(String shopSort) {
        this.shopSort = shopSort;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
    
    
    
    public void changePageSize(ValueChangeEvent e){
        int pageSize = (Integer)e.getNewValue();
        log.log(Level.FINE, "changePageSize:{0}", pageSize);
        setPageSize(pageSize);
        recreateModel();
        recreatePagination();
        getPagination();
    }
    
      public void setProductSort(){
        
        //String newShopSort = (String)e.getNewValue();
        log.log(Level.FINE, "pageSize:{0}", pageSize);
        log.log(Level.FINE, "orderBy:{0}", orderBy);
        log.log(Level.FINE, "orderByField:{0}", orderByField);
        log.log(Level.FINE, "shopSort:{0}", shopSort);
        log.log(Level.FINE, "page:{0}", page);
        String delim = "_";
        
        String[] myvals = shopSort.split(delim);
             
        orderBy = myvals[1];        
        setOrderByField(myvals[0]);
        
        items = getPagination().createPageDataModel();
    }
    
    @RolesAllowed("admin")
    public void processFileUpload(String file) throws FileNotFoundException, IOException{
        CSVReader reader = new CSVReader(new FileReader(file), '\t',CSVParser.DEFAULT_QUOTE_CHARACTER,1);
        //ColumnPositionMappingStrategy strat = new ColumnPositionMappingStrategy();
        //String[] columns = new String[]{"name","description","sku_number","price","quantity","product_categories"};
        //strat.setColumnMapping(columns);
        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            log.log(Level.FINE, "{0}{1}{2}", new Object[]{nextLine[0], nextLine[1], nextLine[2]});
            Product product = new Product();
            product.setName(nextLine[0]);
            product.setDescription(nextLine[1]);
            product.setSkuNumber(nextLine[2]);
            product.setPrice(new BigDecimal(nextLine[3]));
            product.setQuantity(Integer.parseInt(nextLine[4]));
            String[] strProductCategories = nextLine[5].split(",");
            //do something to set the product categories for this product
            List<ProductCategory> prodCats = new ArrayList<ProductCategory>();
            for(String strProdCat : strProductCategories){
                log.fine(strProdCat);                
                ProductCategory pCat = null;
                pCat = pCatFacade.getByLowerCaseName(pCat, strProdCat);
                prodCats.add(pCat);
            }
            product.setCreateDate(new Date() );
            product.setProductCategories(prodCats);
            getFacade().create(product);
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Products Processed"));
    }
    
}
