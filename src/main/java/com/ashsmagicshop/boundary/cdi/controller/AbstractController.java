/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi.controller;

import com.ashsmagicshop.control.jsf.view.Pagination;
import com.ashsmagicshop.control.jsf.view.PaginationHelper;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.faces.model.DataModel;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
public abstract class AbstractController extends Pagination implements Serializable{
    
    private DataModel items = null;   
    private PaginationHelper pagination;
    private int selectedItemIndex;    
    private int page;    
    private int pageSize;    
    private int numberOfPages;    
    private String orderBy;        
    private String orderByField;    
    private String shopSort;    
    private String view;
    
   
    
    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }

    public String getShopSort() {
        return shopSort;
    }

    public void setShopSort(String shopSort) {
        this.shopSort = shopSort;
    }

       
    
    
    
}
