package com.ashsmagicshop.boundary.cdi.controller;

import com.ashsmagicshop.entity.CustomerOrder;
import com.ashsmagicshop.boundary.cdi.controller.util.JsfUtil;
import com.ashsmagicshop.control.jsf.view.PaginationHelper;
import com.ashsmagicshop.boundary.facade.CustomerOrderFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

@Named("customerOrderController")
@SessionScoped
public class CustomerOrderController implements Serializable {

    private CustomerOrder current;
    private DataModel items = null;
    @Inject
    private com.ashsmagicshop.boundary.facade.CustomerOrderFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String view;
    private String outcome;
    private int page;
    private int pageSize;
    private int numberOfPages;
    private String sortOrder;
    private String orderByField;
    
    @Inject
    Conversation convo;
    
    @Inject
    private transient Logger log;

    public CustomerOrderController() {
    }
    
    @PostConstruct
    public void init(){
        log.fine("init called");
        convo.begin();
        this.setView("List.xhtml");
        this.setOutcome("customerOrder");
        this.setPage(1);
        this.setPageSize(10);
        this.setSortOrder("desc");
        this.setOrderByField("customerorderId");
    }

    public CustomerOrder getSelected() {
        if (current == null) {
            current = new CustomerOrder();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CustomerOrderFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        log.fine("getPagination");
        if (pagination == null) {
            log.fine("paginationNull");
            pagination = new PaginationHelper(page-1,pageSize) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    int lastRangeItem = getPageFirstItem() + getPageSize()-1;
                    if(getPageFirstItem() == 0){
                        lastRangeItem = pageSize-1;
                    }
                    log.fine("listDataModel:" + sortOrder);
                    
                    return new ListDataModel(
                            getFacade().findRange(new int[]{getPageFirstItem(), lastRangeItem},
                                    sortOrder,
                                    orderByField));
                }
            };
        }
        //casting the ints to doubles is important!
        //.5s weren't rounding up without it
        double numberOfPages = Math.round( ((double)pagination.getItemsCount()/ (double)pageSize));
        setNumberOfPages((int) numberOfPages);
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CustomerOrder) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        setView("View.xhtml");
        return "index";
    }

    public String prepareCreate() {
        current = new CustomerOrder();
        selectedItemIndex = -1;
        setView("Create.xhtml");
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/customerOrder").getString("CustomerOrderCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/customerOrder").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (CustomerOrder) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        this.setView("Edit.xhtml");
        return "customerOrder";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/customerOrder").getString("CustomerOrderUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/customerOrder").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (CustomerOrder) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/customerOrder").getString("CustomerOrderDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/customerOrder").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CustomerOrder getCustomerOrder(java.lang.Integer id) {
        return ejbFacade.find(id);
    }
    
    public List<CustomerOrder> getCustomerOrders(String field, String sortOrder){
        return ejbFacade.getCustomerOrders("c.customerOrderId", "DESC");
       
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
    
    public void viewAction(){
        log.fine("content:" + this.view);
        log.fine("page:" + this.page);
        getPagination().setPage(page-1);
        log.fine("getPagination.getPage:" + getPagination().getPage());
        items = getPagination().createPageDataModel();
    }
    
    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
    
     public Conversation getConvo() {
        return convo;
    }

    public void setConvo(Conversation convo) {
        this.convo = convo;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }
    
    
    @FacesConverter(forClass = CustomerOrder.class)
    public static class CustomerOrderControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CustomerOrderController controller = (CustomerOrderController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "customerOrderController");
            return controller.getCustomerOrder(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CustomerOrder) {
                CustomerOrder o = (CustomerOrder) object;
                return getStringKey(o.getCustomerorderId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CustomerOrder.class.getName());
            }
        }

    }

}
