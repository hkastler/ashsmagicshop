package com.ashsmagicshop.boundary.cdi.controller;

import com.ashsmagicshop.entity.Product;
import com.ashsmagicshop.boundary.cdi.controller.util.JsfUtil;
import com.ashsmagicshop.control.jsf.view.PaginationHelper;
import com.ashsmagicshop.boundary.facade.ProductFacade;
import com.ashsmagicshop.entity.ProductCategory;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import com.ashsmagicshop.boundary.facade.ProductCategoryFacade;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;


@SessionScoped
@Named( value = "productController")
public class ProductController implements Serializable {

    private Product current;
    private DataModel items = null;
    @Inject
    private com.ashsmagicshop.boundary.facade.ProductFacade productFacade;
    
    @Inject
    private ProductCategoryFacade pCatFacade;
    
    @Inject
    private transient Logger log;
    
    
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private int page;    
    private int pageSize;    
    private int numberOfPages;    
    private String orderBy;        
    private String orderByField;    
    private String shopSort;    
    private String view;
    
    private String defaultOutcome = "productAdmin";
    
   

    public ProductController() {
    }
    
    @PostConstruct
    public void init(){
        log.fine("init called");
        page=1;
        pageSize=10;
        orderBy="asc";
        orderByField="productId";
        shopSort="name_asc";
        view="List.xhtml";
    }

    public Product getSelected() {
        if (current == null) {
            current = new Product();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ProductFacade getFacade() {
        return productFacade;
    }

    
    public PaginationHelper getPagination() {
        log.info("pagination here");
        log.fine("page:" + page);
        if (pagination == null) {
            log.info("paginationNull");
            pagination = new PaginationHelper(page-1,pageSize) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    
                    int lastRangeItem = getPageFirstItem() + getPageSize()-1;
                    if(getPageFirstItem() == 0){
                        lastRangeItem = pageSize-1;
                    }
                    
                    log.fine("firstRangeItem:" + getPageFirstItem());
                    log.fine("lastRangeItem:" + lastRangeItem);
                    /*
                    log.fine("orderBy:" + orderBy);
                    log.fine("orderByField:" + orderByField);
                    log.fine("page:" + getPage());
                    log.fine("pageSize:" + pageSize);
                    log.fine("itemsCount:" + getItemsCount());
                    */
                    return new ListDataModel(
                            getFacade().findRange(
                                    new int[]{getPageFirstItem(), lastRangeItem},
                                    orderBy,
                                    orderByField));
                }
                
            };
        }
        //casting the ints to doubles is important!
        //.5s weren't rounding up without it
        double numberOfPages = Math.round( ((double)pagination.getItemsCount()/ (double)pageSize));
        /*
        log.fine("numberOfPages:" + numberOfPages);
        log.fine("itemsCount:" + pagination.getItemsCount());
        log.fine("pageSize:" + pageSize);
        */
        setNumberOfPages((int) numberOfPages);
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        setView("List.xhtml");        
        return defaultOutcome;
    }

    public String prepareView() {        
        current = (Product) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        setView("View.xhtml");
        return defaultOutcome;
    }
    
     public String prepareView(int productId) {
        log.fine("prepareView.productId:" + productId);
        current = (Product) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        setView("View.xhtml");
        return defaultOutcome;
    }
     
     public String prepareView(int productId, Product product) {
        current = product;
        setView("View.xhtml");
        return defaultOutcome;
    }
    

    public String prepareCreate() {
        current = new Product();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/messages").getString("ProductCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Product) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        setView("Edit.xhtml");
        return defaultOutcome;
    }
    
    public String prepareEdit(Product product) {
        current = product;
        
        setView("Edit.xhtml");
        return "Edit";
    }

    @RolesAllowed("admin")
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/messages").getString("ProductUpdated"));
            setView("View.xhtml");
            return defaultOutcome;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    @RolesAllowed("admin")
    public String destroy() {
        current = (Product) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/productAdmin").getString("ProductDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/productAdmin").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return defaultOutcome;
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return defaultOutcome;
    }

    public void viewAction() {
        log.fine("viewAction here");
        log.fine("page:" + page);
        recreatePagination();
        recreateModel();
    }

    @FacesConverter(forClass = Product.class)
    public static class ProductControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProductController controller = (ProductController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "productController");
            return controller.productFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Product) {
                Product o = (Product) object;
                return getStringKey(o.getProductId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Product.class.getName());
            }
        }

    }
    
    public List<Product> findAll(){
        //log.fine("finding all products");
        List<Product> products = productFacade.findAll();
        return products;
    }
    
    public int getNumberOfProducts(){
        int num = productFacade.count();
        return num;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

   
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    
    

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }

    public String getShopSort() {
        return shopSort;
    }

    public void setShopSort(String shopSort) {
        this.shopSort = shopSort;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
    
    public void processFileUpload(String file) throws FileNotFoundException, IOException{
        CSVReader reader = new CSVReader(new FileReader(file), '\t',CSVParser.DEFAULT_QUOTE_CHARACTER,1);
        //ColumnPositionMappingStrategy strat = new ColumnPositionMappingStrategy();
        //String[] columns = new String[]{"name","description","sku_number","price","quantity","product_categories"};
        //strat.setColumnMapping(columns);
        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            log.fine(nextLine[0] + nextLine[1] + nextLine[2]);
            Product product = new Product();
            product.setName(nextLine[0]);
            product.setDescription(nextLine[1]);
            product.setSkuNumber(nextLine[2]);
            product.setPrice(new BigDecimal(nextLine[3]));
            product.setQuantity(Integer.parseInt(nextLine[4]));
            String[] strProductCategories = nextLine[5].split(",");
            //do something to set the product categories for this product
            List<ProductCategory> prodCats = new ArrayList<ProductCategory>();
            for(String strProdCat : strProductCategories){
                log.fine(strProdCat);                
                ProductCategory pCat = null;
                pCat = pCatFacade.getByLowerCaseName(pCat, strProdCat);
                prodCats.add(pCat);
            }
            product.setCreateDate(new Date() );
            product.setProductCategories(prodCats);
            getFacade().create(product);
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Products Processed"));
    }
    
}
