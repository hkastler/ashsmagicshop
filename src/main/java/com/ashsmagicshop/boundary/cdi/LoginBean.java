/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.UserFacade;
import com.ashsmagicshop.control.util.PasswordUtil;
import com.ashsmagicshop.entity.User;
import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author henry.kastler
 */
@ManagedBean
@ViewScoped
public class LoginBean implements Serializable{

    private String username;
    private String password;
    
    public static final String USER_SESSION_KEY = "user";
    
    @Inject
    private UserFacade uf;
    
    @Inject
    private transient Logger log;
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * <p>Validates the user.  If the user doesn't exist or the password
     * is incorrect, the appropriate message is added to the current
     * <code>FacesContext</code>.  If the user successfully authenticates,
     * navigate them to the page referenced by the outcome <code>app-main</code>.
     * </p>
     *
     */
    public void validateUser() throws ServletException {   
        FacesContext context = FacesContext.getCurrentInstance();
        User user = getUser();
        boolean isPasswordOK = false;
        
        String hashPwd = password;
        log.log(Level.INFO,"password is:" + hashPwd);
        try {
            hashPwd = PasswordUtil.getDigestedPassword(password);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
        }
                
        if (user != null && user.getPassword().equals(hashPwd)) {
            context.getExternalContext().getSessionMap().put(USER_SESSION_KEY, user);
            
            log.info("user now authenticated");
            log.info(user.toString());
            
            //FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) 
                context.getExternalContext().getRequest();
            try {
              request.login(username,password);
              log.info("user authenticated at request level");
            } catch (ServletException e) {
              // e.getStackTrace();
              context.addMessage(null, new FacesMessage("Login failed." + e.getMessage()));
              // return "common/error";
            }
            
            log.log(Level.INFO, "roles:{0}", Arrays.toString(user.getJdbcrealmGroups().toArray()));
            log.log(Level.INFO, "user?{0}", request.isUserInRole("user"));
            log.log(Level.INFO, "admin?{0}", request.isUserInRole("admin"));
            
            FacesContext facesContext = FacesContext.getCurrentInstance();
            String outcome = "success"; // Do your thing?
            facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
            
            
        } else {     
            FacesMessage message = null;
            if(!isPasswordOK){
                 message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Login Failed!",
                    "Username '"
                    + username
                    +
                    "' does not exist.");
            }else{
                message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Login Failed!",
                    "Username '"
                    + username
                    +
                    "' supplied wrong password.");
            }
            context.addMessage(null, message);
            //return null;
        }
    }
    
    private User getUser() {
        try {
            User user = uf.findByUsername(username);
            //Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "user:{0}", user.toString());
            return user; 
        } catch (NoResultException nre) {
            return null;
        }
    }
    
      /**
     * <p>When invoked, it will invalidate the user's session
     * and move them to the homepage.</p>
     *
     * @return <code>home</code>
     */
    public String logout() {
        HttpSession session = (HttpSession)
             FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            session.invalidate();
        }
        
        //HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        return "home";
        
    }
    
}
