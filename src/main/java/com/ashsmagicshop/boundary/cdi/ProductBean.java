/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.ProductFacade;
import com.ashsmagicshop.entity.Product;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@ManagedBean
@RequestScoped
public class ProductBean {
    
    @ManagedProperty("#{param.productId}")
    private int productId;
    
    private Product product;
    
    @Inject
    private ProductFacade pf;
    /**
     * Creates a new instance of ProductBean
     */
    public ProductBean() {
        
    }
    
    @PostConstruct
    private void init(){
        setProduct(pf.find(productId));
       
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
    
    
    
}
