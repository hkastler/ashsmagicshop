/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.ProductFacade;
import com.ashsmagicshop.control.jsf.view.PaginationHelper;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@Named(value = "catalogBean")
@ViewScoped
public class CatalogBean implements Serializable {
    
    private int page;    
    private int pageSize;    
    private int numberOfPages;    
    private String orderBy;       
    private String orderByField;    
    private String shopSort;
    
    private DataModel items = null;
    @Inject
    private ProductFacade ejbFacade;
    private PaginationHelper pagination;
    
    @Inject
    private transient Logger log;
    /**
     * Creates a new instance of CatalogBean
     */
    public CatalogBean() {
    }
    
    @PostConstruct
    public void init(){
        log.info("init called");
        page=1;
        pageSize=12;
        orderBy="asc";
        orderByField="name";
        shopSort="name_asc";
    }
    
    public PaginationHelper getPagination() {
        
        if (pagination == null) {
            log.fine("paginationNull");
            pagination = new PaginationHelper(page-1,pageSize) {

                @Override
                public int getItemsCount() {
                    return getEjbFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    
                    int lastRangeItem = getPageFirstItem() + getPageSize()-1;
                    if(getPageFirstItem() == 0){
                        lastRangeItem = pageSize-1;
                    }
                    log.log(Level.FINE, "firstRangeItem:{0}", getPageFirstItem());
                    log.log(Level.FINE, "lastRangeItem:{0}", lastRangeItem);
                    log.log(Level.FINE, "orderBy:{0}", orderBy);
                    log.log(Level.FINE, "orderByField:{0}", orderByField);
                    log.log(Level.FINE, "page:{0}", getPage());
                    log.log(Level.FINE, "pageSize:{0}", pageSize);
                    log.log(Level.FINE, "itemsCount:{0}", getItemsCount());
                    return new ListDataModel(
                            getEjbFacade().findRange(
                                    new int[]{getPageFirstItem(), lastRangeItem},
                                    orderBy,
                                    orderByField));
                }
                
            };
        }
        //casting the ints to doubles is important!
        //.5s weren't rounding up without it
        double numberOfPages = Math.round( ((double)pagination.getItemsCount()/ (double)pageSize));
        log.log(Level.FINE, "numberOfPages:{0}", numberOfPages);
        log.log(Level.FINE, "itemsCount:{0}", pagination.getItemsCount());
        log.log(Level.FINE, "pageSize:{0}", pageSize);
        setNumberOfPages((int) numberOfPages);
        return pagination;
    }
    
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }

    public String getShopSort() {
        return shopSort;
    }

    public void setShopSort(String shopSort) {
        this.shopSort = shopSort;
    }

     public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    public void setItems(DataModel items) {
        this.items = items;
    }

    public ProductFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(ProductFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    
     public void catalogViewAction(){
        
        //String newShopSort = (String)e.getNewValue();
        log.log(Level.FINE, "pageSize:{0}", pageSize);
        log.log(Level.FINE, "orderBy:{0}", orderBy);
        log.log(Level.FINE, "orderByField:{0}", orderByField);
        log.log(Level.FINE, "shopSort:{0}", shopSort);
        log.log(Level.FINE, "page:{0}", page);
        String delim = "_";
        
        
        String[] myvals = shopSort.split(delim);             
        orderBy = myvals[1];        
        setOrderByField(myvals[0]);
        
        getPagination();
    }
    
    
    
}
