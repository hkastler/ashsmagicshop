/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author henry.kastler
 */
@Named
@ApplicationScoped
public class Index {

    String title = "Welcome to Ash's Magic Shop";
    String metaDescription = "magic, magic tricks, card tricks, coin tricks, mentalism";
    Map<String, String> local = new HashMap<String, String>();
    String registerLink = "";
    
    /**
     * Creates a new instance of Index
     */
    public Index() {
    }
    
    @PostConstruct
    public void init(){
        //this.setTitle("Hey there");
        local.put("registerLink", "register");
        local.put("signUpReturn", "index");
        local.put("shopLink","shop");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Map<String, String> getLocal() {
        return local;
    }

    public void setLocal(Map<String, String> local) {
        this.local = local;
    }

    public String getRegisterLink() {
        return registerLink;
    }

    public void setRegisterLink(String registerLink) {
        this.registerLink = registerLink;
    }
    
    
    
}
