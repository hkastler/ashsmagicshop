/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.control.mail.EmailSessionBean;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author henry.kastler
 */
@Named(value = "bookMagicShowBean")
@RequestScoped
public class BookMagicShowBean implements Serializable{
    
    private String name = new String();
    private String emailAddress = new String();
    private String eventType = new String();
    private Date dateTimeOfEvent;
    //Date dateTimeOfEventAsDate;
    private String strDateTimeOfEvent;
    private String description = new String();
    private boolean isEmailSent = false;
    
    @Inject
    Logger log;
    
    @Inject
    EmailSessionBean emailBean;
    /**
     * Creates a new instance of BookMagicShowBean
     */
    public BookMagicShowBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
    /*
    public String getDateTimeOfEvent() {
        if(dateTimeOfEvent==null){
             DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:00");
             Date now = new Date();
             dateTimeOfEvent = format.format(now);
        }
        return dateTimeOfEvent;
    }

    public void setDateTimeOfEvent(String dateTimeOfEvent) {
        this.dateTimeOfEvent = dateTimeOfEvent;
    }
    */
    public Date getDateTimeOfEvent() throws ParseException{
        
        return this.dateTimeOfEvent ;
    }
    
    public void setDateTimeOfEvent(Date myDate) throws ParseException{
        
        this.dateTimeOfEvent = myDate;
    }

    public String getStrDateTimeOfEvent() {
        return strDateTimeOfEvent;
    }

    public void setStrDateTimeOfEvent(String strDateTimeOfEvent) {
        this.strDateTimeOfEvent = strDateTimeOfEvent;
    }
    
    
   
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isIsEmailSent() {
        return isEmailSent;
    }

    public void setIsEmailSent(boolean isEmailSent) {
        this.isEmailSent = isEmailSent;
    }
    
    
    
    public void bookAction() throws ParseException, MessagingException{
        
        log.log(Level.INFO, "name:{0}", getName());
        log.log(Level.INFO, "emailAddress:{0}", getEmailAddress());
        //log.log(Level.INFO, "date:{0}", getDateTimeOfEvent());
        //log.info("dateTimeOfEventAsDate:" + dateTimeOfEvent.toString());
        log.info("strDateTimeOfEvent:" + strDateTimeOfEvent);
        log.log(Level.INFO, "description:{0}", getDescription());
        String emailBody = "Mr Ash Show Request\r\n";
        emailBody += "Name: ".concat(getName()).concat("\r\n");
        emailBody += "Email Address: ".concat(getEmailAddress()).concat("\r\n");
        emailBody += "Date and Time Requested: ".concat(getStrDateTimeOfEvent()).concat("\r\n");
        emailBody += "Description: ".concat(getDescription());
        
        emailBean.sendEmail(getEmailAddress(), "Mr Ash Show Request", emailBody);
        log.log(Level.INFO, "email sent");
        setIsEmailSent(true);
    }
    
    
}
