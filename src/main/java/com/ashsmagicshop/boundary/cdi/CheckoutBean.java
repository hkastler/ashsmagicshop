/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.CustomerOrderFacade;
import com.ashsmagicshop.boundary.facade.ShippingServiceFacade;
import com.ashsmagicshop.boundary.cdi.controller.util.JsfUtil;
import com.ashsmagicshop.control.cart.ShoppingCart;
import com.ashsmagicshop.entity.Address;
import com.ashsmagicshop.entity.CustomerOrder;
import com.ashsmagicshop.entity.OrderPaymentMethod;
import com.ashsmagicshop.entity.ShippingService;
import java.io.IOException;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;


/**
 *
 * @author henry.kastler
 */

@Named(value = "checkoutBean")
@ConversationScoped
public class CheckoutBean implements Serializable {
    
    @Inject
    CartBean cartBean;
    
    @Inject
    private transient Logger log;
    
    @Inject
    private Conversation convo;
    
    ShoppingCart cart;
    CustomerOrder customerOrder;
    private OrderPaymentMethod orderPaymentMethod;    
    
    private Address billingAddress;    
    private String billingTelephone;
    
    private Address shippingAddress;    
    private String shippingTelephone;
    
    private ShippingService shippingService;

    public ShippingService getShippingService() {
        return shippingService;
    }

    public void setShippingService(ShippingService shippingService) {
        this.shippingService = shippingService;
    }
    
    
    private String orderComments;
    
    private BigDecimal subtotal;
    
    private boolean checkoutComplete;
    //private BigDecimal shippingCost;
    private int shippingServiceId;
   
    //private BigDecimal taxAmount;
    //private BigDecimal grandTotal;
    @Inject
    private CustomerOrderFacade orderFacade;
    
    @Inject
    private ShippingServiceFacade ssf;
     
    
    /**
     * Creates a new instance of CheckoutBean
     */
    public CheckoutBean() {
    }
    
    @PostConstruct
    private void init(){
      log.log(Level.FINEST,"I'm initing");
      
      //redirect if no items//
      if(cartBean.getCart().getNumberOfItems()==0){
         ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + "/shop/cart.xhtml");
        } catch (IOException ex) {
            log.severe(ex.getMessage());
        }
      }
      
      
      convo.begin();
      setCheckoutComplete(false);      
      setCartBean(cartBean);
      setCart(cartBean.getCart());
      setTotals();
      customerOrder = new CustomerOrder();
      customerOrder.setSubTotal(cartBean.getCart().getSubtotal());
      customerOrder.setShippingCharge(new BigDecimal(0));
      customerOrder.setTaxAmount(new BigDecimal(0));
      
      customerOrder.setGrandTotal(cartBean.getCart().getTotal());
      
      billingAddress = new Address();
      shippingAddress = new Address();
      
      orderPaymentMethod = new OrderPaymentMethod();
      
      //taxAmount = new BigDecimal(0);
      //grandTotal = cartBean.getCart().getTotal();
      //customerOrder.setBillingAddress(billingAddress);
      
      shippingService = new ShippingService();
      shippingServiceId = -1;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void checkout(){
        log.log(Level.INFO,"checkout initiated");
         
        
        List<Address> shippingAddresses = new ArrayList<Address>();
        shippingAddresses.add(shippingAddress);
        this.customerOrder.setShippingAddresses(shippingAddresses);  
        
       orderPaymentMethod.setCustomerOrder(this.customerOrder);
       orderPaymentMethod.setPaymentAmount(this.customerOrder.getGrandTotal());
       this.customerOrder.getOrderPaymentMethods().add(orderPaymentMethod);
       
       //testing only
       //shippingService.setShippingServiceId(1);
       this.customerOrder.setShippingService(shippingService);
       
       //cOrder.setShippingCharge(shippingCost);
       //cOrder.setTaxAmount(taxAmount);
       //this.customerOrder.setSubTotal(cart.getSubtotal());
       //cOrder.setGrandTotal();
       this.customerOrder.setOrderDate(new Date());
        //try{
            orderFacade.create(this.customerOrder);
            
            log.log(Level.INFO, "cOrder:{0} created", this.customerOrder.getCustomerorderId());
            log.log(Level.INFO, "cOrder.orderPaymentMethod:{0} created", this.customerOrder.getOrderPaymentMethods().get(0).toString());
            cartBean.getCart().clear();
            setCheckoutComplete(true);
        //}catch(Exception e){
            //e.getStackTrace();
        //}
        //JsfUtil.addSuccessMessage("Your order has been posted, and is in the tubes now");
        
        
        //String returnPath = "complete.xhtml";
        
        //returnPath += "?faces-redirect=true";
        /*
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            context.getExternalContext().redirect(returnPath);
        } catch (IOException ex) {
            log.severe(ex.getMessage());
        }
        */
    }
    
    public void setShippingFromBilling(){
       log.log(Level.FINEST,"hello from shipping from billing");
        shippingAddress.setAddressId(billingAddress.getAddressId());
        shippingAddress = orderFacade.createOrReturnAddress(shippingAddress);
        
    }
    
    public void billingStateCodeChange(ValueChangeEvent event){
        //String newValue = e.getNewValue().toString();
       // log.log(Level.INFO, "newValue:{0}", newValue);
        
        /*
        log.log(Level.INFO, "phaseId:{0}", event.getPhaseId().getName());
        if (event.getPhaseId() != PhaseId.INVOKE_APPLICATION) {
            event.setPhaseId(PhaseId.INVOKE_APPLICATION);
            event.queue();
            return;
        }
        
        log.log(Level.INFO, "phaseId:{0}", event.getPhaseId().getName());
        if(customerOrder.getBillingAddress().getStateCode().equals("IL")){
            customerOrder.setTaxAmount(new BigDecimal(5.00));
            customerOrder.recalcGrandTotal();
        }
        */
    }
    
    public void billingStateCodeChange(AjaxBehaviorEvent event){
        log.log(Level.INFO, "phaseId:{0}", event.getPhaseId().getName());
        log.info("billingStateCodeChange here");
        FacesContext context = FacesContext.getCurrentInstance();
        Object val = ((UIOutput)event.getSource()).getValue().toString();
        log.log(Level.INFO, "state:{0}", val);
        log.log(Level.INFO, "state:{0}", this.customerOrder.getBillingAddress().getStateCode());
        if(customerOrder.getBillingAddress().getStateCode().equals("IL")){
            customerOrder.setTaxAmount(new BigDecimal(5.00));
            
        }else{
            customerOrder.setTaxAmount(new BigDecimal(0.00));
        }
        log.log(Level.INFO, "customerOrder.subTotal:{0}", customerOrder.getSubTotal());
        log.log(Level.INFO, "customerOrder.taxAmount:{0}", customerOrder.getTaxAmount());
        log.log(Level.INFO, "customerOrder.shippingCharge:{0}", customerOrder.getShippingCharge());
        BigDecimal newGrandTotal = customerOrder.calcGrandTotal(customerOrder.getSubTotal(),
                                                                customerOrder.getShippingCharge(),
                                                                customerOrder.getTaxAmount());
        customerOrder.setGrandTotal(newGrandTotal);
        
    }
    
    public void valueChangeListenerMethod(ValueChangeEvent event) {
        
        log.log(Level.INFO, "phaseId:{0}", event.getPhaseId().getName());
        if (event.getPhaseId() != PhaseId.UPDATE_MODEL_VALUES) {
            event.setPhaseId(PhaseId.UPDATE_MODEL_VALUES);
            event.queue();
            return;
        }
        log.info("valueChangeListenerMethod!");
        // Do your original job here. 
        // It will only be invoked when current phase ID is INVOKE_APPLICATION.
        log.log(Level.INFO, "shippingService is:{0}", shippingService.toString());
        this.getCustomerOrder().setShippingService(shippingService);
        this.getCustomerOrder().setShippingCharge(BigDecimal.ZERO);
        //BigDecimal shippingCost = this.getCustomerOrder().getShippingCharge().add();
        this.getCustomerOrder().setShippingCharge(shippingService.getCost());
        log.log(Level.INFO, "grandTotal:{0}", this.getCustomerOrder().getGrandTotal());
        log.log(Level.INFO, "subTotal:{0}", this.getCustomerOrder().getSubTotal());
        log.log(Level.INFO, "shippingCharge:{0}", this.getCustomerOrder().getShippingCharge());
        log.log(Level.INFO, "taxAmount:{0}", this.getCustomerOrder().getTaxAmount());
        this.getCustomerOrder().recalcGrandTotal();
        log.log(Level.INFO, "shippingCost:{0}", this.getCustomerOrder().getShippingCharge());
        log.log(Level.INFO, "grandTotal:{0}", this.getCustomerOrder().getGrandTotal());
    }
    
    public void shippingMethodChange(AjaxBehaviorEvent event){
    
        log.log(Level.INFO, "shippingService:{0}", shippingService.getName());
        //FacesContext context = FacesContext.getCurrentInstance();
        //Object val =  this.shippingService;
        //log.log(Level.INFO, "ss:{0}", val.toString());
        //if(val.equals("IL")){
           // customerOrder.setTaxAmount(new BigDecimal(5.00));
           // customerOrder.setGrandTotal(customerOrder.getGrandTotal().add(customerOrder.getTaxAmount()));
        //}
        //log.info("taxAmount:" + customerOrder.getTaxAmount());
        //log.info("grandTotal:" + customerOrder.getGrandTotal());
          //this.setShippingService(ss);
        
          this.getCustomerOrder().setShippingService(shippingService);
          this.getCustomerOrder().setShippingCharge(new BigDecimal(0));
          BigDecimal shippingCost = this.getCustomerOrder().getShippingCharge().add(shippingService.getCost());
          this.getCustomerOrder().setShippingCharge(shippingCost);
          log.log(Level.INFO, "grandTotal:{0}", this.getCustomerOrder().getGrandTotal());
          this.customerOrder.recalcGrandTotal();
          log.log(Level.INFO, "shippingCost:{0}", this.getCustomerOrder().getShippingCharge());
          log.log(Level.INFO, "grandTotal:{0}", this.getCustomerOrder().getGrandTotal());
        
          //FacesContext context = FacesContext.getCurrentInstance();
          //PartialViewContext partialView = context.getPartialViewContext();
         // partialView.getRenderIds().add("form:grandTotal, form:shippingCharge");
         // context.renderResponse();
    }
    
    public List<ShippingService> getShippingServices(){
        List<ShippingService> services = new ArrayList<ShippingService>();
        //ShippingService select0 = new ShippingService(-1,"Select",null);
        //services.add(select0);
        services.addAll(ssf.findAll());
        return services;
    }
    
     public void getShippingServiceForZip(AjaxBehaviorEvent event) {
        log.info("getShippingService here");
        
        log.log(Level.INFO, "zip:{0}", "");
        String zipCode = this.shippingAddress.getStateCode();
        log.log(Level.INFO, "zip:{0}", zipCode);
        if(zipCode.length()>= 5){
            //zipCode = this.customerOrder.getBillingAddress().getStateCode();
        }else{
            log.log(Level.INFO,"zip is only {0} characters",zipCode.length());
        }
    }
     
    public void useBillingForShipping(AjaxBehaviorEvent event) {
        log.info("useBillingForShipping here");
        log.log(Level.INFO, "firstName:{0}", this.customerOrder.getBillTofirstName());
        this.customerOrder.setShipTofirstName(this.customerOrder.getBillTofirstName());
        this.customerOrder.setShipTolastName(this.customerOrder.getBillTolastName());
        this.customerOrder.getShippingAddresses().add(this.customerOrder.getBillingAddress());
        //log.log(Level.INFO, "zip:{0}", "");
        //String zipCode = this.shippingAddress.getStateCode();
        //log.log(Level.INFO, "zip:{0}", zipCode);
        //if(zipCode.length()>= 5){
            //zipCode = this.customerOrder.getBillingAddress().getStateCode();
       // }else{
        //    log.log(Level.INFO,"zip is only {0} characters",zipCode.length());
       // }
    }
    
    public void applyCoupon(){
        
    }
    
    public CustomerOrderFacade getOrderFacade() {
        return orderFacade;
    }

    public void setOrderFacade(CustomerOrderFacade orderFacade) {
        this.orderFacade = orderFacade;
    }

    public CartBean getCartBean() {
        return cartBean;
    }

    public void setCartBean(CartBean cartBean) {
        this.cartBean = cartBean;
       // this.cart = cartBean.getCart();
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cartBean.getCart();
    }
  

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
    
    public String getBillingTelephone() {
        return billingTelephone;
    }

    public void setBillingTelephone(String billingTelephone) {
        this.billingTelephone = billingTelephone;
    }

    public String getShippingTelephone() {
        return shippingTelephone;
    }

    public void setShippingTelephone(String shippingTelephone) {
        this.shippingTelephone = shippingTelephone;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }
    
    public OrderPaymentMethod getOrderPaymentMethod() {
        return orderPaymentMethod;
    }

    public void setOrderPaymentMethod(OrderPaymentMethod orderPaymentMethod) {
        this.orderPaymentMethod = orderPaymentMethod;
    }
    
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    private void setTotals() {
        //this.getCustomerOrder().setSubTotal(cart.getSubtotal());
        //this.getCustomerOrder().setGrandTotal(cart.getTotal());
    }

    public String getOrderComments() {
        return orderComments;
    }

    public void setOrderComments(String orderComments) {
        this.orderComments = orderComments;
    }

    public ShippingServiceFacade getSsf() {
        return ssf;
    }

    public void setSsf(ShippingServiceFacade ssf) {
        this.ssf = ssf;
    }
    
    
    public boolean isCheckoutComplete() {
        return checkoutComplete;
    }

    public void setCheckoutComplete(boolean checkoutComplete) {
        this.checkoutComplete = checkoutComplete;
    }

    public int getShippingServiceId() {
        return shippingServiceId;
    }

    public void setShippingServiceId(int shippingServiceId) {
        this.shippingServiceId = shippingServiceId;
    }

    
    
}
