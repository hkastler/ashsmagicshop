/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.EmailsignupFacade;
import com.ashsmagicshop.boundary.cdi.controller.util.JsfUtil;
import com.ashsmagicshop.entity.Emailsignup;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henry.kastler
 */
@Named
@RequestScoped
public class EmailSignUpBean {

    private String emailAddress;
    private String firstName;
    private String lastName;
    private boolean isSignedUp = false;
    
    @Inject
    private EmailsignupFacade eupf;
    
    @Inject
    private transient Logger log;
    
    /**
     * Creates a new instance of EmailSignUp
     */
    public EmailSignUpBean() {
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isIsSignedUp() {
        return isSignedUp;
    }

    public void setIsSignedUp(boolean isSignedUp) {
        this.isSignedUp = isSignedUp;
    }
    
    
    
    public String signUp(String returnPath){
        
        log.info("signUp");
        log.log(Level.INFO, "email:{0}", getEmailAddress());
         
        Emailsignup eup = new Emailsignup();
        eup.setEmailAddress(getEmailAddress());
        eup.setFirstName(getFirstName());
        eup.setLastName(getLastName());
        
        eupf.create(eup);
        setIsSignedUp(true);
        JsfUtil.addSuccessMessage("You're all signed up");
        
        
        returnPath += "?faces-redirect=true";
        return returnPath;
    }
    
    public void signUp(){
        
        log.info("signUp");
        log.log(Level.INFO, "email:{0}", getEmailAddress());
         
        Emailsignup eup = new Emailsignup();
        eup.setEmailAddress(getEmailAddress());
        eup.setFirstName(getFirstName());
        eup.setLastName(getLastName());
        
        eupf.create(eup);
        setIsSignedUp(true);
        JsfUtil.addSuccessMessage("You're all signed up");
        log.info("newsletter sign up");
        
        
    }
    
    
}
