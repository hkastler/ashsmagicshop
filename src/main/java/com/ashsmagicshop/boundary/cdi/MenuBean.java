/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.entity.ShippingService;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@Named(value = "menuBean")
@RequestScoped
public class MenuBean {

    /**
     * Creates a new instance of MenuBean
     */
    public MenuBean() {
    }
    
    @Inject
    private transient Logger log;
    
    ShippingService selectService = new ShippingService(-1,"Select",null);
    ShippingService preService = new ShippingService(-1,"Enter shipping address zip code to see rates",null);
    private SelectItem[] functions = {
        new SelectItem(selectService, selectService.getName()),
        new SelectItem(preService, preService.getName())
        
    };
    ShippingService noService = new ShippingService(0,"Please Select",null);
    ShippingService ground = new ShippingService(1,"Ground",new BigDecimal(5));
    ShippingService secondDay = new ShippingService(2,"2nd Day Air",new BigDecimal(10));
    ShippingService nextDay = new ShippingService(3,"Next Day Air",new BigDecimal(50));
    
    private SelectItem[] shippingFunctions = {
            new SelectItem(noService, noService.getName()),            
            new SelectItem(ground,ground.getName()),
            new SelectItem(secondDay,secondDay.getName()),
            new SelectItem(nextDay,nextDay.getName())
    };

    public SelectItem[] getFunctions() {
        return functions;
    }

    private void switchSelectedFunctions(int n) {
        log.info("switching funcions");
        setFunctions(shippingFunctions);
    }

    public void setFunctions(SelectItem[] function) {
        this.functions = function;
    }

    public void getShippingServiceForZip(ValueChangeEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        
        int value = Integer.valueOf(((String) event.getNewValue()));
        log.finest("value:" + event.getNewValue());
        switchSelectedFunctions(value);
        context.renderResponse();
    }
    
    public void getShippingServiceForZip(AjaxBehaviorEvent event) {
        log.info("getShippingService here");
        FacesContext context = FacesContext.getCurrentInstance();
        Object val = ((UIOutput)event.getSource()).getValue().toString();
        log.log(Level.INFO, "zip:{0}", val);
        String zipCode = (String) ((UIOutput)event.getSource()).getValue();
        if(zipCode.length()>= 5){
            switchSelectedFunctions(Integer.parseInt(zipCode));
            PartialViewContext partialView = FacesContext.getCurrentInstance().getPartialViewContext();
            partialView.getRenderIds().add("form:shippingService");
            context.renderResponse();
        }else{
            log.log(Level.INFO,"zip is only {0} characters",zipCode.length());
        }
    }
    
    
    
}
