/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author henry.kastler
 */
@Named(value = "templateBean")
@ApplicationScoped
public class TemplateBean {

    /**
     * Creates a new instance of TemplateBean
     */
    public TemplateBean() {
    }
    
}
