/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author henry.kastler
 */
@Named(value = "contactUsBean")
@RequestScoped
public class ContactUsBean {

    private String name;
    private String emailAddress;
    private String message;
    
    @Inject
    private transient Logger log;
    
    /**
     * Creates a new instance of ContactUsBean
     */
    public ContactUsBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public void contactUsFormAction(){
        log.info("hello from contact us bean");
        log.log(Level.FINE, "name:{0}", name);
        log.log(Level.FINE, "email:{0}", emailAddress);
    }
    
    
    
}
