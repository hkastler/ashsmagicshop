/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author henry.kastler
 */
@Named(value = "navigationBean")
@RequestScoped
public class NavigationBean {

    /**
     * Creates a new instance of NavigationBean
     */
    public NavigationBean() {
    }
    
}
