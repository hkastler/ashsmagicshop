/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.cdi.controller.ProductController;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;

/**
 *
 * @author henry.kastler
 */
@ManagedBean(name = "fileUploadBean")
@RequestScoped
@MultipartConfig(location="C:\\websites\\uploads", fileSizeThreshold=1024*1024,
    maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
public class FileUploadBean {
    
    @Inject
    ProductController productController;
    
    //@Inject 
    private static final Logger log = Logger.getLogger(FileUploadBean.class.getName());
    
    private Part file;
    
    private boolean uploaded = false;    
    private boolean preserveOriginalFilename = true;
    
    @PostConstruct
    public void init(){
        setUploaded(false);
        setPreserveOriginalFilename(true);
    }
    
    public void upload(){
        log.info("call upload...");      
        log.log(Level.INFO, "content-type:{0}", file.getContentType());
        log.log(Level.INFO, "filename:{0}", file.getName());
        log.log(Level.INFO, "submitted filename:{0}", file.getSubmittedFileName());
        
        log.log(Level.INFO, "size:{0}", file.getSize());
        String writFilename = "C:\\websites\\uploads\\";
        if(preserveOriginalFilename){
            writFilename += file.getSubmittedFileName();
        }else{
           
            int extIdx = file.getName().lastIndexOf(".");
            if( extIdx != -1){
                String fileExt = file.getName().substring(extIdx+1);
                log.log(Level.INFO,"fileExt:{0}",fileExt);
                String fileName = file.getName().split(":")[0].toString();
                log.log(Level.INFO,"fileName: {0}", fileName);
                writFilename += fileName + "." + fileExt;
            }
            
        }
        log.log(Level.INFO,"writFilename:{0}",writFilename);
        try {
            
            byte[] results=new byte[(int)file.getSize()];
            InputStream in=file.getInputStream();
            in.read(results);
            OutputStream out = new FileOutputStream(writFilename);
            out.write(results);
            out.close();
            log.log(Level.INFO,writFilename + " written to disk");
            setUploaded(true);
            log.log(Level.INFO,"file uploaded");
            
            productController.processFileUpload(writFilename);
            
        } catch (IOException ex) {
            log.log(Level.SEVERE, " ex @{0}", ex);
        }
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("File Uploaded"));
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean isUploaded) {
        this.uploaded = isUploaded;
    }

    public boolean isPreserveOriginalFilename() {
        return preserveOriginalFilename;
    }

    public void setPreserveOriginalFilename(boolean preserveOriginalFilename) {
        this.preserveOriginalFilename = preserveOriginalFilename;
    }
    
    
    
}
