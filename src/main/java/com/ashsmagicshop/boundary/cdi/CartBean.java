/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.cdi;

import com.ashsmagicshop.boundary.facade.ProductFacade;
import com.ashsmagicshop.control.cart.ShoppingCart;
import com.ashsmagicshop.entity.Product;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;


/**
 *
 * @author henry.kastler
 */
@SessionScoped
@Named("cartBean")
public class CartBean implements Serializable {
    
    private String template = "/WEB-INF/templates/site/template.xhtml";
    
    private ShoppingCart cart;
    
    @Inject
    ProductFacade productFacade;
    
   @Inject
   private transient Logger log;
            
    /**
     * Creates a new instance of CartBean
     */
    public CartBean() {
    }
    
    @PostConstruct
    public void init(){
        this.cart = new ShoppingCart();
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }
    
    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
    
    public String updateCartItem(Product product, String quantity){
        //Product product = productFacade.find(Integer.parseInt(productId));
        log.log(Level.INFO, "updateCartItem called, quantity:{0}", quantity);
       
        if(quantity.equals(""))quantity="0";
        cart.update(product, quantity);   
        cart.calculateTotal("0");
        return "cart";
    }
    
    public void updateCartItem(){
        String productId = getParam("productId");
        log.log(Level.INFO,"productId:" + productId);
        String quantity = getParam("qty");
        log.log(Level.INFO, "qty:{0}", quantity);
        //Product product = productFacade.find(Integer.parseInt(productId));
        //updateCartItem(product,quantity);
    }
    
    protected String getParam(String param) {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> map = context.getExternalContext().getRequestParameterMap();
        return map.get(param);
    }
    
    public void valueChangeCartItem(ValueChangeEvent e){
        Object obj = e.getNewValue();
        Object src = e.getSource();
        
        log.log(Level.INFO,src.toString());
        
        log.log(Level.INFO, "newCartItem:{0}", obj.toString());
        
    }
    
    public void updateCart(){
        
    }
}
