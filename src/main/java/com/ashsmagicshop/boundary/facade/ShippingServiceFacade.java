/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.ShippingService;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class ShippingServiceFacade extends AbstractFacade<ShippingService> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ShippingServiceFacade() {
        super(ShippingService.class);
    }
    
}
