/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.ProductCategory;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class ProductCategoryFacade extends AbstractFacade<ProductCategory> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public ProductCategoryFacade() {
        super(ProductCategory.class);
    }
    
    public ProductCategory getByLowerCaseName(ProductCategory pCat, String strProdCat){
        
        String jql = "SELECT pc FROM ProductCategory pc WHERE lower(pc.name)=:pCatName";
        
        try{
                pCat = (ProductCategory) getEntityManager().createQuery(jql)
                                                                .setParameter("pCatName", strProdCat.toLowerCase())
                                                                .getSingleResult();
                }catch(NoResultException nre){
                    //continue;
                }
                if(pCat == null){
                    pCat = new ProductCategory();
                    pCat.setName(strProdCat);
                    create(pCat);
                }
        return pCat;
    }
    
}
