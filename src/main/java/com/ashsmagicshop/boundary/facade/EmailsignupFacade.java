/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.Emailsignup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class EmailsignupFacade extends AbstractFacade<Emailsignup> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmailsignupFacade() {
        super(Emailsignup.class);
    }
    
    public void create(String firstName, String lastName, String emailAddress){
        Emailsignup em = new Emailsignup();
        em.setEmailAddress(emailAddress);
        em.setFirstName(firstName);
        em.setLastName(lastName);
        super.create(em);
    }
    
}
