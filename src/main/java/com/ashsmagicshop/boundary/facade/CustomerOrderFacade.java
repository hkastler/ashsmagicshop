/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.Address;
import com.ashsmagicshop.entity.CustomerOrder;
import com.ashsmagicshop.entity.OrderPaymentMethod;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class CustomerOrderFacade extends AbstractFacade<CustomerOrder> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;
    
    @Inject
    private transient Logger log;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerOrderFacade() {
        super(CustomerOrder.class);
    }
    
    public List<CustomerOrder> getCustomerOrders(String orderByField, String sortOrder){
        String jql = "SELECT c FROM CustomerOrder as c ORDER BY ";
        jql = jql.concat(orderByField).concat(" ").concat(sortOrder);
        Query sortQuery = em.createQuery(jql);
        List<CustomerOrder> results = sortQuery.getResultList();
        return results;
    }
    
    public Address createOrReturnAddress(Address address){
        if(address.getAddressId() == null){
            em.persist(address);
        }else{
           address = em.find(Address.class,address.getAddressId());
        }
        return address;
    }
    
    
    public OrderPaymentMethod createOrFind(OrderPaymentMethod opm){
        if(opm.getOrderPaymentMethodId() == null){
            em.persist(opm);
            em.flush();
            log.fine("new opm:" + opm.getOrderPaymentMethodId());
        }else{
           opm = em.find(OrderPaymentMethod.class, opm.getOrderPaymentMethodId());
           
        }
        return opm;
    }
}
