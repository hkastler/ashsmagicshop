/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.OrderPaymentMethod;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class OrderPaymentMethodFacade extends AbstractFacade<OrderPaymentMethod> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderPaymentMethodFacade() {
        super(OrderPaymentMethod.class);
    }
    
    public OrderPaymentMethod createOrFind(OrderPaymentMethod opm){
        if(opm.getOrderPaymentMethodId() == null){
            super.create(opm);
            System.out.println("new opm:" + opm.getOrderPaymentMethodId());
        }else{
           opm = super.find(opm.getOrderPaymentMethodId());
           
        }
        return opm;
    }
}
