/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.ProductHasProductcategory;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class ProductHasProductcategoryFacade extends AbstractFacade<ProductHasProductcategory> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductHasProductcategoryFacade() {
        super(ProductHasProductcategory.class);
    }
    
}
