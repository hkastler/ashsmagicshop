/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashsmagicshop.boundary.facade;

import com.ashsmagicshop.entity.JdbcrealmGroup;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author henry.kastler
 */
@Stateless
public class JdbcrealmGroupFacade extends AbstractFacade<JdbcrealmGroup> {
    @PersistenceContext(unitName = "ashsmagicshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public JdbcrealmGroupFacade() {
        super(JdbcrealmGroup.class);
    }
    
    public List<JdbcrealmGroup> findByName(String groupname){
        List<JdbcrealmGroup> jg = em.createNamedQuery("JdbcrealmGroup.findByGroupname")
                .setParameter("groupname", groupname).getResultList();
        return jg;
    }
    
}
