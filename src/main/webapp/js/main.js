function toggler(divId) {
    console.log("toggler here");
    console.log(divId);
    $("#" + divId).toggle();
}

function showRegionOrState(value){
    console.log("value:" + value);
    var stateCode = $("#form\\:shippingStateCode");
    console.log(stateCode.val());
    var stateText = $("#form\\:shippingStateText");
    if(value=="US"){
       if(stateCode.hasClass("hidden")){
            stateCode.removeClass("hidden");
            stateCode.addClass("show");
            
            stateText.toggleClass("hidden show");
        }
    }else{
        if(stateText.hasClass("hidden")){
            stateText.removeClass("hidden");
            stateText.addClass("show");
            
            stateCode.removeClass("show");
            stateCode.addClass("hidden");
        }
    }
}

$(document).ready(function () {
  $('[data-toggle="offcanvas"]').click(function () {
    $('.row-offcanvas').toggleClass('active');
    $('#sidebarChevron').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
  });
  
  $('#pageSizeCtrl').change(function(){
    var pageSize = $(this).val();
    console.log("pageSize:" + pageSize);
    var urlAry = window.location.href.split("?");
    var currentURL = urlAry[0];
    var currentQs = urlAry[1];
    //currentURL += "?pageSize=" + pageSize;
    var qs = $.param({pageSize:pageSize});
    currentURL += "?" + qs;
    console.log(currentURL);
    window.location.href = currentURL;
    //document.form.submit();
   });
});

