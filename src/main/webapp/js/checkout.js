/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$('#checkoutForm\\:billingPostalCode').on('change', function() {
    //alert( $(this).val());
    $('#checkoutForm\\:useBillingAddressForShipping').val($(this).val());
 });

$('#checkoutForm\\:shippingPostalCode').on('change', function() {
    //alert( $(this).val());
    $('#shipping-method-fieldset').removeAttr("disabled");
});

$('#checkoutForm\\:useBillingAddressForShipping').on('click',function(){
   $('#address-billing-fieldset').copyNamedTo("billing","shipping");
   //$("#address-shipping-fieldset").prop("disabled",$(this).is(':checked'));
   enableShipping();
});

function enableShipping(){
    if($('#checkoutForm\\:shippingPostalCode').val().length < 5){
        $('#shipping-method-fieldset').attr('disabled',true);
    }else{
        $('#shipping-method-fieldset').removeAttr("disabled");
    }
}

// A $( document ).ready() block.
$( document ).ready(function() {
    enableShipping();
});

(function($) {
    $.fn.copyNamedTo = function(copyfrom,copyto) {
        return this.each(function() {
            $(':input[name]', this).each(function() {
                
                var name = $(this).attr('name');
                if(name.startsWith('checkoutForm:billing')){
                    console.log(name);
                    var copytoname = name.replace(copyfrom,copyto);
                    console.log(copytoname);
                    console.log($(this).val());
                    console.log($(this).attr("type"))
                    $('[name="' + copytoname + '"]').val($(this).val());
                    $('[name="' + copytoname + '"]').attr("selected", $(this).attr("selected"));
                    //$('[name=' + name +']', shipname).val($(this).val());
                    
                }
                
                //$('[name=' + $(this).attr('name') +']', other).attr("checked", $(this).attr("checked"));
            })  
        });
    };
}(jQuery));


function transferBillingToShipping(selected){
    var billingZip = $("#form\\:billingPostalCode").val();
    console.log("billingZip:" + billingZip);
    if(selected){
        //$("#shipping-vendor-methods").html("dropdown with prices based on cart and zip");
        $("#form\\:shippingPostalCode").val(billingZip);
        
        console.log("shipZip:" + $("#form\\:shippingPostalCode").val());
    }else{
        //$("#shipping-vendor-methods").html("Enter your zip code in the Shipping ");
    }
    //console.log("billingZip:" + billingZip);
    $("#address-shipping-fieldset").prop("disabled",selected);
}

if (typeof String.prototype.startsWith != 'function') {
  // see below for better implementation!
  String.prototype.startsWith = function (str){
    return this.indexOf(str) === 0;
  };
}